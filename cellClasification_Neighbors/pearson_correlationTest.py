# -*- coding: utf-8 -*-
"""
Created on Thu Dec 11 09:58:21 2014

@author: pedro-xubuntu
"""

"""To test pearson correlection"""

import scipy.stats as stats
import numpy as np
import os
import itertools
import networkx as nx
import random
#file=np.genfromtxt(os.getcwd()+'/COX_Analysis_and_Result_Cell_NC_vs_Change3orMore/D2_All_4_norm_.csv',delimiter=',',skiprows=1,dtype=None)
"""dtypes = np.dtype({ 'names' : ('prop0', 'prop1', 'prop2', 'prop3', 'prop4', 'prop5', 'prop6', 'prop7', 'prop8', 'prop9', 'prop10', 'prop11', 'prop12', 'prop13', 'prop14', 'prop15', 'prop16', 'prop17', 'prop18', 'prop19', 'prop20','Class'),

                   'formats' : [np.float, np.float, np.float,np.float,
                                np.float, np.float, np.float, np.float,
                                np.float, np.float, np.float, np.float,
                                np.float, np.float, np.float, np.float,
                                np.float, np.float, np.float, np.float, np.float,'S10'] })
file=np.loadtxt(os.getcwd()+'/COX_Analysis_and_Result_Cell_NC_vs_Change3orMore/D2_All_4_norm_.csv',delimiter=',',skiprows=1,dtype=dtypes)"""

def buildMatrixDistance(neighbourInit):
    file=open(os.getcwd()+'/COX_Analysis_and_Result_Cell_NC_vs_Change3orMore/D2_All_'+str(neighbourInit)+'_norm_.csv')
    file=file.readlines()
    dataChange=list()
    dataNoChange=list()
    for line in file[1:]:
        lineSplit=line.replace('\n','').split(',')
        if lineSplit[-1]=='Change':
            r=[float(i)  for i in lineSplit[:-1]]
            r.append(lineSplit[-1])
            #r=np.array(r)
            dataChange.append(r)
        else:
            r=[float(i)  for i in lineSplit[:-1]]
            r.append(lineSplit[-1])
            #r=np.array(r)
            dataNoChange.append(r)

    allCells=dataChange+dataNoChange
    comb=itertools.combinations(range(len(allCells)),2)
    res=[[0]*len(allCells) for _ in range(len(allCells))]

    for x,y in comb:
        corr,p=stats.pearsonr(np.array(allCells[x][:-1]),np.array(allCells[y][:-1]))
        res[x][y]=corr

    #f2_norm=open('MatrixDistance_'+str(neighbourInit)+'_.csv', mode='x')

    """for i in res:
        f2_norm.write(str(i).replace('[','').replace(']',))
    f2_norm.close()"""
    np.savetxt('MatrixDistance_'+str(neighbourInit)+'_.csv',np.array(res),delimiter=',')

def drawGraph_withTreshold(tresh):
    g=nx.Graph()
    g.add_nodes_from(range(len(allCells)))

    for row in range(len(allCells)):
        for column in range(row+1,len(allCells)):
            if res[row][column]>=tresh:
                g.add_edge(row,column)

    nx.draw(g,node_color=['Red']*len(dataNoChange)+['Blue']*len(dataChange))