Call:
coxph(formula = Surv(Class) ~ Relation_Neighbour_Ratio_Axis + 
    Clustering_Coefficient + Relation_Neighbour_Angle + Area_Descriptor + 
    Relation_Neighbour_Major_Axis + Convex_Hull + Betweenness + 
    Relation_Neighbour_Area + Area + Area_Normalized + Average_NoN + 
    angle + Ratio_axis + Relation_Neighbour_Convex_Hull + Eccentricity + 
    Ratio_Descriptor + Minor_axis + Relation_Neighbour_Minor_Axis + 
    Major_axis + Strength + Centroid_Descriptor, data = d)

  n= 1320, number of events= 1320 

                                     coef  exp(coef)   se(coef)      z Pr(>|z|)    
Relation_Neighbour_Ratio_Axis   2.1034903  8.1947221  0.4875398  4.314 1.60e-05 ***
Clustering_Coefficient          0.0304712  1.0309401  0.4388509  0.069 0.944644    
Relation_Neighbour_Angle       -0.1939635  0.8236880  0.9030596 -0.215 0.829935    
Area_Descriptor                -0.2156137  0.8060466  0.2041035 -1.056 0.290788    
Relation_Neighbour_Major_Axis  -1.7841356  0.1679422  0.7828885 -2.279 0.022672 *  
Convex_Hull                     2.0222675  7.5554378  0.9347246  2.163 0.030504 *  
Betweenness                     0.5405324  1.7169207  0.2316907  2.333 0.019649 *  
Relation_Neighbour_Area         1.3291495  3.7778289  1.0413803  1.276 0.201837    
Area                            4.2410356 69.4797664  1.7967151  2.360 0.018253 *  
Area_Normalized                 4.2063048 67.1081023  1.4747229  2.852 0.004341 ** 
Average_NoN                    -0.6528962  0.5205360  0.1933325 -3.377 0.000733 ***
angle                          -0.0001483  0.9998517  0.0951639 -0.002 0.998757    
Ratio_axis                     -3.0413088  0.0477723  1.2708571 -2.393 0.016706 *  
Relation_Neighbour_Convex_Hull -1.5553002  0.2111260  0.8845050 -1.758 0.078682 .  
Eccentricity                    0.3301577  1.3911875  0.1978705  1.669 0.095206 .  
Ratio_Descriptor                0.1743747  1.1905015  0.3030959  0.575 0.565080    
Minor_axis                     -2.3651412  0.0939360  2.2007683 -1.075 0.282514    
Relation_Neighbour_Minor_Axis  -1.9864747  0.1371782  0.8983169 -2.211 0.027013 *  
Major_axis                     -1.6644837  0.1892884  1.7550446 -0.948 0.342926    
Strength                       -3.9733285  0.0188107  0.8708934 -4.562 5.06e-06 ***
Centroid_Descriptor            -0.4245137  0.6540878  0.1755038 -2.419 0.015571 *  
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

                               exp(coef) exp(-coef) lower .95 upper .95
Relation_Neighbour_Ratio_Axis    8.19472    0.12203  3.151662   21.3073
Clustering_Coefficient           1.03094    0.96999  0.436197    2.4366
Relation_Neighbour_Angle         0.82369    1.21405  0.140306    4.8356
Area_Descriptor                  0.80605    1.24062  0.540290    1.2025
Relation_Neighbour_Major_Axis    0.16794    5.95443  0.036205    0.7790
Convex_Hull                      7.55544    0.13236  1.209541   47.1953
Betweenness                      1.71692    0.58244  1.090272    2.7037
Relation_Neighbour_Area          3.77783    0.26470  0.490702   29.0848
Area                            69.47977    0.01439  2.053486 2350.8505
Area_Normalized                 67.10810    0.01490  3.728108 1207.9846
Average_NoN                      0.52054    1.92110  0.356357    0.7604
angle                            0.99985    1.00015  0.829721    1.2049
Ratio_axis                       0.04777   20.93262  0.003957    0.5767
Relation_Neighbour_Convex_Hull   0.21113    4.73651  0.037295    1.1952
Eccentricity                     1.39119    0.71881  0.943970    2.0503
Ratio_Descriptor                 1.19050    0.83998  0.657256    2.1564
Minor_axis                       0.09394   10.64554  0.001258    7.0166
Relation_Neighbour_Minor_Axis    0.13718    7.28979  0.023585    0.7979
Major_axis                       0.18929    5.28294  0.006071    5.9023
Strength                         0.01881   53.16118  0.003413    0.1037
Centroid_Descriptor              0.65409    1.52885  0.463710    0.9226

Concordance= 0.776  (se = 0.025 )
Rsquare= 0.104   (max possible= 1 )
Likelihood ratio test= 145  on 21 df,   p=0
Wald test            = 133.3  on 21 df,   p=0
Score (logrank) test = 133.8  on 21 df,   p=0

