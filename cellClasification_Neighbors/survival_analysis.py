# -*- coding: utf-8 -*-
"""
Created on Fri Nov 21 12:34:04 2014

@author: pedro-xubuntu
"""

import numpy as np
import os
import pprint

def generate_filesToCoxR_Analysis_VariousFilesNotChange_OneFileChange(neighbourNumbers):

    prop='Area_Normalized, Area, Major_axis, Minor_axis, Ratio_axis, Convex_Hull, angle, Relation_Neighbour_Area, Relation_Neighbour_Major_Axis, Relation_Neighbour_Minor_Axis, Relation_Neighbour_Ratio_Axis, Relation_Neighbour_Convex_Hull, Relation_Neighbour_Angle, Strength, Clustering_Coefficient, Eccentricity, Betweenness, Average_NoN, Centroid_Descriptor, Area_Descriptor, Ratio_Descriptor,Class\n'


    f_Change=open('/home/pedro-xubuntu/Workspace/cell-knowloedge/cellClasification_Neighbors/COX_Analysis_and_Result_Cell_NC_vs_Change1orMore/'+str(neighbourNumbers)+'Change_vs_'+str(neighbourNumbers)+'NotChange/D2_change_'+str(neighbourNumbers)+'_norm.csv')
    f_Change=f_Change.readlines()
    for i in range(0,15):
        f_notChange=open('/home/pedro-xubuntu/Workspace/cell-knowloedge/cellClasification_Neighbors/COX_Analysis_and_Result_Cell_NC_vs_Change1orMore/'+str(neighbourNumbers)+'Change_vs_'+str(neighbourNumbers)+'NotChange/D2_not_change_'+str(neighbourNumbers)+'_norm_'+str(i)+'.csv')
        f_notChange=f_notChange.readlines()
        f_all=open('/home/pedro-xubuntu/Workspace/cell-knowloedge/cellClasification_Neighbors/COX_Analysis_and_Result_Cell_NC_vs_Change1orMore/'+str(neighbourNumbers)+'Change_vs_'+str(neighbourNumbers)+'NotChange/D2_'+str(neighbourNumbers)+'_'+str(i)+'.csv',mode='x')

        f_all.write(prop)

        for lineNC in f_notChange[1:]:
            f_all.write(str(lineNC).replace('[','').replace(']','').replace('\n','')+',0\n')

        for lineC in f_Change[1:]:
            f_all.write(str(lineC).replace('[','').replace(']','').replace('\n','')+',1\n')
        f_all.close()

def generate_filesToCoxR_Analysis_OneFileNotChange_VariousFilesChange(neighbourNumbers):

    prop='Area_Normalized, Area, Major_axis, Minor_axis, Ratio_axis, Convex_Hull, angle, Relation_Neighbour_Area, Relation_Neighbour_Major_Axis, Relation_Neighbour_Minor_Axis, Relation_Neighbour_Ratio_Axis, Relation_Neighbour_Convex_Hull, Relation_Neighbour_Angle, Strength, Clustering_Coefficient, Eccentricity, Betweenness, Average_NoN, Centroid_Descriptor, Area_Descriptor, Ratio_Descriptor,Class\n'


    f_notChange=open('/home/pedro-xubuntu/Workspace/cell-knowloedge/cellClasification_Neighbors/COX_Analysis_and_Result_Cell_NC_vs_Change1orMore/'+str(neighbourNumbers)+'Change_vs_'+str(neighbourNumbers)+'NotChange/D2_not_change_'+str(neighbourNumbers)+'_norm.csv')
    f_notChange=f_notChange.readlines()
    for i in range(0,15):
        f_Change=open('/home/pedro-xubuntu/Workspace/cell-knowloedge/cellClasification_Neighbors/COX_Analysis_and_Result_Cell_NC_vs_Change1orMore/'+str(neighbourNumbers)+'Change_vs_'+str(neighbourNumbers)+'NotChange/D2_change_'+str(neighbourNumbers)+'_norm_'+str(i)+'.csv')
        f_Change=f_Change.readlines()
        f_all=open('/home/pedro-xubuntu/Workspace/cell-knowloedge/cellClasification_Neighbors/COX_Analysis_and_Result_Cell_NC_vs_Change1orMore/'+str(neighbourNumbers)+'Change_vs_'+str(neighbourNumbers)+'NotChange/D2_'+str(neighbourNumbers)+'_'+str(i)+'.csv',mode='x')

        f_all.write(prop)

        for lineNC in f_notChange[1:]:
            f_all.write(str(lineNC).replace('[','').replace(']','').replace('\n','')+',0\n')

        for lineC in f_Change[1:]:
            f_all.write(str(lineC).replace('[','').replace(']','').replace('\n','')+',1\n')
        f_all.close()

def generate_filesToCoxR_Analysis_OneFileNotChange_VariousFilesChange_AllCells():

    prop='Area_Normalized, Area, Major_axis, Minor_axis, Ratio_axis, Convex_Hull, angle, Relation_Neighbour_Area, Relation_Neighbour_Major_Axis, Relation_Neighbour_Minor_Axis, Relation_Neighbour_Ratio_Axis, Relation_Neighbour_Convex_Hull, Relation_Neighbour_Angle, Strength, Clustering_Coefficient, Eccentricity, Betweenness, Average_NoN, Centroid_Descriptor, Area_Descriptor, Ratio_Descriptor,Class\n'


    f_notChange=open('/home/pedro-xubuntu/Workspace/cell-knowloedge/cellClasification_Neighbors/COX_Analysis_and_Result_Cell_NC_vs_Change1orMore_NoFilter/D2_not_change_All_norm.csv')
    f_notChange=f_notChange.readlines()
    for i in range(0,15):
        f_Change=open('/home/pedro-xubuntu/Workspace/cell-knowloedge/cellClasification_Neighbors/COX_Analysis_and_Result_Cell_NC_vs_Change1orMore_NoFilter/D2_change_All_norm_'+str(i)+'.csv')
        f_Change=f_Change.readlines()
        f_all=open('/home/pedro-xubuntu/Workspace/cell-knowloedge/cellClasification_Neighbors/COX_Analysis_and_Result_Cell_NC_vs_Change1orMore_NoFilter/D2_'+str(i)+'.csv',mode='x')

        f_all.write(prop)

        for lineNC in f_notChange[1:]:
            f_all.write(str(lineNC).replace('[','').replace(']','').replace('\n','')+',0\n')

        for lineC in f_Change[1:]:
            f_all.write(str(lineC).replace('[','').replace(']','').replace('\n','')+',1\n')
        f_all.close()

def analyze_CoxResults_FilteringByN_SortByCoef_OneFile(neighbourNumber):


    file=open(os.getcwd()+'/COX_Analysis_and_Result_Cell_NC_vs_Change3orMore/'+str(neighbourNumber)+'Change_vs_'+str(neighbourNumber)+'NotChange/CoxAnalysisResult_'+str(neighbourNumber)+'N.txt')
    datas= file.readlines()

    coef_1star=dict() #Key: PropiertieName - Value: Coef vale
    coef_2star=dict() #Key: PropiertieName - Value: Coef vale
    coef_3star=dict() #Key: PropiertieName - Value: Coef vale


    """Now we parser datas and get relevant lines:
        lines: [12:32] : PropiertieName (0) - Coef (1) - exp(Coef) (2) - se(Coef) (3) - z (4) - Pr(<|z|) (5) - Stars (6)
    """

    coef_list=[i.split() for i in datas[12:32]]
    for i in coef_list:

        if i.__len__()==7 and i[6]!='.':
                """Filter by stars in order to add in the correct dict"""
                if i[6].__len__()==1: #1 stars
                    if i[0] not in coef_1star:
                        coef_1star[i[0]]=float(i[1])
                    else:
                        n=coef_1star[i[0]]+float(i[1])
                        coef_1star[i[0]]=n

                elif i[6].__len__()==2:# 2 Stars
                    if i[0] not in coef_2star:
                        coef_2star[i[0]]=float(i[1])
                    else:
                        n=coef_2star[i[0]]+float(i[1])
                        coef_2star[i[0]]=n

                elif i[6].__len__()==3: #3 Stars
                    if i[0] not in coef_3star:
                        coef_3star[i[0]]=float(i[1])
                    else:
                        n=coef_3star[i[0]]+float(i[1])
                        coef_3star[i[0]]=n

    """Sort dict by value, Coef, It return a list with key order by Coef"""
    sortedProp_1star=sorted(coef_1star,key=coef_1star.get,reverse=True)
    sortedProp_2star=sorted(coef_2star,key=coef_2star.get,reverse=True)
    sortedProp_3star=sorted(coef_3star,key=coef_3star.get,reverse=True)

    print('1 Stars')
    pprint.pprint([(i,coef_1star[i]) for i in sortedProp_1star])
    print('2 Stars')
    pprint.pprint([(i,coef_2star[i]) for i in sortedProp_2star])
    print('3 Stars')
    pprint.pprint([(i,coef_3star[i]) for i in sortedProp_3star])


def analyze_CoxResults_FilteringByN_SortByProp_ManyFiles(neighbourNumber):

    path=os.getcwd()+'/COX_Analysis_and_Result_Cell_NC_vs_Change3orMore/'+str(neighbourNumber)+'Change_vs_'+str(neighbourNumber)+'NotChange/CoxAnalysisResult_'+str(neighbourNumber)+'N_'

    res_1=dict() #Key: Stars - Value: Set of Propierties

    for i in range(15):

        file=open(path+str(i)+'.txt')
        datas= file.readlines()

        """Now we parser datas and get relevant lines:
            lines: [12:35] : PropiertieName - Coef - exp(Coef) - se(Coef) - z - Pr(<|z|)
            lines: [34:54] : PropiertieName - Stars
        """
        coef_list=[i.split() for i in datas[12:32]]

        for i in coef_list:
            if i.__len__()==7 and  i[6]!='.':

                    if i[1] not in res_1:
                        res_1[i[6]]=set([i[0]])
                    else:
                        s=res_1[i[6]]
                        s.add(i[0])
                        res_1[i[6]]=s
    pprint.pprint(res_1)


def analyze_CoxResults_FilteringByN_SortByStars_ManyFiles(neighbourNumber):

    path=os.getcwd()+'/COX_Analysis_and_Result_Cell_NC_vs_Change3orMore/'+str(neighbourNumber)+'Change_vs_'+str(neighbourNumber)+'NotChange/CoxAnalysisResult_'+str(neighbourNumber)+'N_'


    res_2=dict() #Key: Stars - Values: list([]) Each position mean stars * ** ***
    for i in range(15):

        file=open(path+str(i)+'.txt')
        datas= file.readlines()

        """Now we parser datas and get relevant lines:
            lines: [12:35] : PropiertieName - Coef - exp(Coef) - se(Coef) - z - Pr(<|z|)
            lines: [34:54] : PropiertieName - Stars
        """

        coef_list=[i.split() for i in datas[12:32]]

        for i in coef_list:
            if i.__len__()==7:

                    if i[0] not in res_2:
                        l=[0,0,0,0]

                        if i[6]=='.':
                            l[0]=1
                        elif i[6]=='*':
                            l[1]=1
                        elif i[6]=='**':
                            l[2]=1
                        elif i[6]=='***':
                            l[3]=1

                        res_2[i[0]]=l
                    else:
                        l=res_2[i[0]]
                        if i[6]=='.':
                            l[0]=1
                        elif i[6]=='*':
                            l[1]=1
                        elif i[6]=='**':
                            l[2]=1
                        elif i[6]=='***':
                            l[3]=1
                        res_2[i[0]]=l


    sl=sorted(res_2.items(), key=lambda e: e[1][3], reverse=True)
    print('Point, 1 Stars, 2 Stars, 3 Stars')
    pprint.pprint(sl)


def analyze_CoxResults_FilteringByN_SortByCoef_ManyFiles(neighbourNumber):

    path=os.getcwd()+'/COX_Analysis_and_Result_Cell_NC_vs_Change3orMore/'+str(neighbourNumber)+'Change_vs_'+str(neighbourNumber)+'NotChange/CoxAnalysisResult_'+str(neighbourNumber)+'N_'
    #path=os.getcwd()+'/COX_Analysis_and_Result_Cell_NC_vs_Change1orMore_NoFilter/CoxAnalysisResult_All_'



    coef_1star=dict() #Key: PropiertieName - Value: Coef vale
    coef_2star=dict() #Key: PropiertieName - Value: Coef vale
    coef_3star=dict() #Key: PropiertieName - Value: Coef vale
    coef_Point=dict()
    for i in range(0,15):

        file=open(path+str(i)+'.txt')
        datas= file.readlines()

        """Now we parser datas and get relevant lines:
            lines: [12:35] : PropiertieName - Coef - exp(Coef) - se(Coef) - z - Pr(<|z|)
            lines: [34:54] : PropiertieName - Stars
        """
        coef_list=[i.split() for i in datas[12:32]]

        for i in coef_list:

            if i.__len__()==7:
                    """Filter by stars in order to add in the correct dict"""
                    if i[6].__len__()==1 and i[6]=='*': #1 stars or point
                        if i[0] not in coef_1star:
                            coef_1star[i[0]]=float(i[1])
                        else:
                            n=coef_1star[i[0]]+float(i[1])
                            coef_1star[i[0]]=n
                    elif i[6].__len__()==1 and i[6]=='.': #1 stars or point
                        if i[0] not in coef_Point:
                            coef_Point[i[0]]=float(i[1])
                        else:
                            n= coef_Point[i[0]]+float(i[1])
                            coef_Point[i[0]]=n
                    elif i[6].__len__()==2:# 2 Stars
                        if i[0] not in coef_2star:
                            coef_2star[i[0]]=float(i[1])
                        else:
                            n=coef_2star[i[0]]+float(i[1])
                            coef_2star[i[0]]=n

                    elif i[6].__len__()==3: #3 Stars
                        if i[0] not in coef_3star:
                            coef_3star[i[0]]=float(i[1])
                        else:
                            n=coef_3star[i[0]]+float(i[1])
                            coef_3star[i[0]]=n

    """Sort dict by value, Coef, It return a list with key order by Coef"""
    sortedProp_1star=sorted(coef_1star,key=coef_1star.get,reverse=True)
    sortedProp_2star=sorted(coef_2star,key=coef_2star.get,reverse=True)
    sortedProp_3star=sorted(coef_3star,key=coef_3star.get,reverse=True)
    sortedProp_Point=sorted(coef_Point,key=coef_Point.get,reverse=True)
    print('1 Stars')
    pprint.pprint([(i,coef_1star[i]) for i in sortedProp_1star])
    print('2 Stars')
    pprint.pprint([(i,coef_2star[i]) for i in sortedProp_2star])
    print('3 Stars')
    pprint.pprint([(i,coef_3star[i]) for i in sortedProp_3star])
    print('Point')
    pprint.pprint([(i,coef_Point[i]) for i in sortedProp_Point])




