# -*- coding: utf-8 -*-
"""
Created on Mon Jan 12 10:51:22 2015

@author: pedro-xubuntu
"""

import networkx as nx
import numpy as np
import numpy.linalg as la
from skimage import io
import matplotlib.pyplot as plt
from scipy.spatial import ConvexHull

"""
#######################################################
This file has 5 function:

    *def build_masterGraph_imageX_eachDiagram(image):
    *def save_smallGraph_Information(list_of_graphs,image):
    *def get_small_graph(masterGraph,vertex):
    *def plot_graph_original_position(graph):
    *def equals(smallGraph1: nx.Graph,smallGraph2: nx.Graph):

#######################################################

    This file has 5 function:

        *def build_masterGraph_imageX_eachDiagram(image: int):
            * Parameter(1):: int: number, that means the number of a image
            * Return list([networkx grpah,networkx grpah...networkx grpah]: The graph list

        *def save_smallGraph_Information(list_of_graphs: list([nx.graph,nx.graph..nxgraph]) ,image: int):
            * Parameter(2)::
                * list([graph,graph..graph]): A list of graphs of one image,
                * int: The number of the image of the list of graph

        *def get_small_graph(masterGraph: nx.Graph ,vertex: tuple):
            * Parameter(2)::
                * networkx graph: A master Graph / big graph
                * Tuple: (x,y), A vertex that is contained in the masterGraph
            * Return networkx grap: the smallGraph

        *def plot_graph_original_position(graph: nx.rRaph):
            * Parameter(1): * networkx graph: A graph that each node has as name the position as tuple (x,y)
            * Return the plot with matplotlib

        *def equals(smallGraph1: nx.Graph,smallGraph2: nx.Graph):
            * Parameter(2)::
                * nx.Graph,nx.Graph
            * Return tuple(Bool, list(),list())
#######################################################
"""

""""
*def build_masterGraph_imageX_eachDiagram(image):

This function build the big graph. The graph of all vertex of a image file This mean that build one graph per diagram (21).
A file contains data of each diagram 1.0,1.25...6.0 and the funciont add each graph in a list, Index:0 - GraphD1.0, 1 - GraphD1.25 .... 20 - GraphD6.0
Also each graph is build only with the valid cells that are in other file

The function takes as parameter one parameter:
    * int: number, that means the number of a image

Return list([networkx grpah,networkx grpah...networkx grpah]: The graph list
"""

def angle(centroid,p2):

    v1=np.array([p2[0]-centroid[0],p2[1]-centroid[1]])
    v2=np.array([1.0,0.0])
    """ Returns the angle in radians between vectors 'v1' and 'v2'    """
    cosang = np.dot(v1, v2)
    sinang = la.norm(v1) * la.norm(v2)
    degree= np.arccos(cosang/sinang)

    if p2[1]>=centroid[1]:
        degree=2*np.pi-degree

    return degree

def polygon_edges(points,center):

    res=list()
    for p in range(len(points)):
        a=angle(center,points[p])
        res.append((p,a))
    res.sort(key=operator.itemgetter(1))
    edges=[(x[0],j[0]) for x,j in zip(res[:-1],res[1:])]
    edges.append((res[0][0],res[-1][0]))
    return np.array(edges)

def build_masterGraph_imageX_eachDiagram(image):
    f=open('VertexData_and_ValidCells/Vertex_Center_Image'+str(image)+'.csv')  #Here open the file that contain the vertex(column-row) in pixel of each cell
    f_data=f.readlines() #Read all files, getting it in a list of list, each line a list
    f.close() #Close the file
    f_data=[i.replace('\n','').split(',') for i in f_data]

    """Get Valid Cells"""
    valid_cells=set()
    f=open('VertexData_and_ValidCells/Matrix_adjacency_Image_'+str(image)+'.csv')
    f_valid_cells=f.readlines()
    f.close()
    f_valid_cells=[i.split(',')  for i in f_valid_cells[2:]]
    for i in f_valid_cells:
        valid_cells.add('Cell_'+i[2])

    f_valid_cells=None

    cellVertexData=dict() #Create the data structure
    """I'm going to storage data in a dict: key-value
        Key: Is cell name in a string, ex: 'Cell_1', or 'Cell_5'
        Value: Is a numpy nd-array that storage the vertex(Col,Row) in pixel of a cell,like that: [ [col,row],[col,row],...[col,row]]
    """

    #camera = io.imread('Datos_imagen_1_Diagrama_1.00.png') #This is the orginal image, for see the cells
    #plt.imshow(camera, cmap='gray', interpolation='nearest') #To plot the image
    col_init=2
    col_end=17
    #diagrams=['1.00', '1.25', '1.50', '1.75', '2.00', '2.25', '2.50', '2.75', '3.00', '3.25', '3.50', '3.75', '4.00', '4.25', '4.50', '4.75', '5.00', '5.25', '5.50', '5.75', '6.00']
    res=list() #List with all the graph of one Image, one per diagram
    for diagram_num in range(21):
        #print(diagram_num)
        #camera = io.imread('Datos_imagen_1_Diagrama_'+diagrams[diagram_num]+'.png') #This is the orginal image, for see the cells
        #plt.imshow(camera, cmap='gray', interpolation='nearest') #To plot the image
        masterGraph= nx.Graph()

        for row_line,col_line in zip(range(2,4002,2),range(3,4002,2)): #Here Iterate over index of the file, because in the file the information is writen for each cell in two lines, even lines - row index, odd lines - column index

            row_line=f_data[row_line][col_init:col_end] #Split the line
            col_line=f_data[col_line][col_init:col_end]

            key_name=row_line[0]+'_'+col_line[0] #Save the name-key

            centroid=(int(col_line[2])-1,int(row_line[2])-1)
            values_vertex_index=[np.array([int(col)-1,int(row)-1]) for col,row in zip(col_line[3:],row_line[3:]) if col!='0' and row!='0']  #creating values-2d-array, avoiding vertex with index 0, this is beacause there are cell with different number of vertex and nodes. Index 0,0 means that no exist/there aren't more vertex

            values_vertex_index=np.array(values_vertex_index) #Convert to numpy array

            """values_vertex_index.append(np.array([int(i)-1 for i in row_line[2:] if i!='0']))
            values_vertex_index.append(np.array([int(j)-1 for j in col_line[2:] if j!='0']))"""

            """for x,y in zip(row_line[2:],col_line[2:]):
                values_vertex_index.append((int(x)-1,int(y)-1))"""

            cellVertexData[key_name]=values_vertex_index #Save into data structure the cell vertex
            plt.scatter(values_vertex_index[:,0],values_vertex_index[:,1], color='r') #This is to plot vertex

            """
            ConvexHull of a set of X points get the smallest convex set that contains X,
            when X is a bounded subset of the plane, the convex hull may be visualized as
            the shape formed by a rubber band stretched around X

            hull = ConvexHull(points)
            Points have to be ndarray [[x,y],[x,y][x,y]...[x,y]]
            hull.simplices and hull.neighbors return the edges/neighbours by index points
            hull.vetices return the vertices by index points
            """

            #hull=ConvexHull(values_vertex_index)
            hull=polygon_edges(values_vertex_index,centroid)

            """
            First Add nodes by its name, the name would be cordenates: X_Y
            And each node/vertex is going to have one propiertie, that it the cells that belong to it vertex. A list with the name or number of the cell
            """
            for v in values_vertex_index:
                if not masterGraph.has_node(tuple(v)):
                    masterGraph.add_node(tuple(v),valid_cells='',no_valid_cells='',valid=False) #Here add nodes to the graph, each node is a vertex of a cells, and each node has the propiertie 'cell' to storage the cell name that
                if key_name in valid_cells:
                    masterGraph.node[tuple(v)]['valid_cells']+=key_name+',' #Adding the cell name to a node
                else:
                    masterGraph.node[tuple(v)]['no_valid_cells']+=key_name+',' #Adding the cell name to a node

            """
            Second add the edge between nodes by the nodes name
            """
            for simplex in hull:#.simplices: #Iterate over the edges, edges are numpy array [x,y], where x,y are the indexs of the points

                v1=tuple(values_vertex_index[simplex[0]])
                v2=tuple(values_vertex_index[simplex[1]])

                masterGraph.add_edge(v1,v2) #adding edge between two vertex
                #plt.plot(values_vertex_index[simplex,0], values_vertex_index[simplex,1], 'k-',linewidth=0.5)
            #plt.annotate(key_name,centroid)
            #plt.show()
        col_init+=17
        col_end+=17
        res.append(masterGraph)
        #d={ (x,y):(x,y) for (x,y) in masterGraph.nodes()}
        #print('Draw')
        #nx.draw(masterGraph,d,node_size=5,width=0.2,edge_color ='b',dpi=900)
        #plt.savefig('test'+str(diagram_num)+'.png',dpi=800)
        #plt.close()

    return res #Return the list that contain all the graphs of each of one image, one per diagram

"""
#######

*def save_smallGraph_Information(list_of_graphs,image):

This function save into a text/csv file the information of a graph/smallGraph:
    *Nodes,
    *Edges
    *Cells that intervene in a smallGraph

The funcion takes two parameter:
    * list([graph,graph..graph]): A list of graphs of one image,
    * int: The number of the image of the list of graph

#######
"""
def save_smallGraph_Information(list_of_graphs,image):

    with open('Small_Graphs_Information_Image_'+str(image)+'.csv', 'x') as f:
        res=list() #list with dict, one per diagram
        for i,master_graph in enumerate(list_of_graphs):
            d=dict()
            f.write('Diagram '+str(i)+'\n')
            nodes_master_graph= master_graph.nodes_iter()
            for num,node in enumerate(nodes_master_graph):
                f.write('Small Graph '+str(num)+' - Node: '+str(node)+' \n')
                sm=get_small_graph(master_graph,node)
                sm_information=list()
                sm_information.append(str(sm.nodes().replace(',','-')))
                sm_information.append(sm.edges())
                sm_information.append([str(sm_node)+': '+str(sm.node[sm_node]['cell']).replace('[','').replace(']','').replace('{','').replace('}','').replace('"','').replace('"','')+', '  for sm_node in sm.nodes_iter()])
                f.write('Nodes:, '+str(sm_information[0]).replace('[','').replace(']','')+'\n')
                f.write('Edges:, '+str(sm_information[1]).replace('[','').replace(']','')+'\n')
                f.write('Cells Envolved:, '+str(sm_information[2]).replace('[','').replace(']','')+'\n')
                d[node]=sm_information
                f.write('####\n')
                f.write('\n')
            res.append(d)

    return res

"""
#######

*def get_small_graph(masterGraph,vertex):

This function build the smallGraph(radio=2) of a vertex contained in a big graph

This function takes two parameter:
    * networkx graph: A master Graph / big graph
    * Tuple: (x,y), A vertex that is contained in the masterGraph:

Return networkx grap: the smallGraph
#######
"""
def get_small_graph(masterGraph,vertex):
    """
    For each node get a subgraph of maximum length 2
    1.a: Get the neighbours of a node
    1.b: Add edges of the neighbours
    2.a: Get neighbours of neighbours
    2.b: Add edges of neighbours of neighbours
    """

    small_graph=nx.Graph() #Create the small graph

#    first_neighbours_list=masterGraph.neighbors(vertex) #1.a Get neighbours of vertex passed
#    first_edges=[(vertex,x) for x in masterGraph.edge[vertex].keys()]
#    small_graph.add_nodes(vertex, cell=list(),valid=None)
#    small_graph.add_nodes_from(first_neighbours_list, cell=list(),valid=None)
#    small_graph.add_edges_from(first_edges)
#
#    small_graph.node[vertex]['cell']=masterGraph.node[vertex]['cell']
#    small_graph.node[vertex]['cell']=masterGraph.node[vertex]['valid']
#    small_graph.node[vertex]['reference']=True
#
#    for node in first_neighbours_list:
#        small_graph.node[node]['cell']=masterGraph.node[node]['cell']
#        small_graph.node[node]['cell']=masterGraph.node[node]['valid']
#
#    second_neigbours_list=list()
#    second_edges_list=list()
#    for node in first_neighbours_list:
#        second_neigbours_list.extend(masterGraph.neighbors(node))

    neighbours_list=masterGraph.neighbors(vertex) #1.a Get neighbours of vertex passed
    small_graph.add_nodes_from(neighbours_list,valid_cells='',no_valid_cells='') #Add nodes to the graph
    edges=[(vertex,k) for k in neighbours_list] #1.b Get edges of neighbours of the vertex passed
    small_graph.add_edges_from(edges) #Add edges to the small graph
    for n in neighbours_list:
        small_graph.node[n]['valid_cells']=masterGraph.node[n]['valid_cells'] #Add propierties cell to each node and also the values of it propierties
        small_graph.node[n]['no_valid_cells']=masterGraph.node[n]['no_valid_cells'] #Add propierties valid to each node and also the values of it propierties
    small_graph.node[vertex]['reference']=True

    """With the last lines, get the small graph of length 1
        Now the second level, length 2
        It is the same, I could do a recursion but how it is simple I repeat the code
    """
    for nextNode in neighbours_list:
        next_neighbours_list=masterGraph.neighbors(nextNode)
        small_graph.add_nodes_from(next_neighbours_list,valid_cells='',no_valid_cells='')
        edges=[(nextNode,i) for i in next_neighbours_list]
        small_graph.add_edges_from(edges)
        for n in next_neighbours_list:
            small_graph.node[n]['valid_cells']=masterGraph.node[n]['valid_cells']
            small_graph.node[n]['no_valid_cells']=masterGraph.node[n]['no_valid_cells']
        for nextNode_2 in next_neighbours_list:
            edges= masterGraph.edge[nextNode_2]
            edges=[(nextNode_2,x) for x in edges.keys() if small_graph.has_node(x)]
        small_graph.add_edges_from(edges)

    return small_graph

def equals(smallGraph1: nx.Graph,smallGraph2: nx.Graph):

    neighbours_number_list_1=list()
    neighbours_number_list_2=list()

    for node in smallGraph1.nodes_iter():
        neighbours_number= len(smallGraph1.neighbors(node))
        neighbours_number_list_1.append(neighbours_number)

    for node in smallGraph2.nodes_iter():
        neighbours_number= len(smallGraph2.neighbors(node))
        neighbours_number_list_2.append(neighbours_number)

    neighbours_number_list_1.sort()
    neighbours_number_list_2.sort()
    res= neighbours_number_list_1 == neighbours_number_list_2

    return res,neighbours_number_list_1,neighbours_number_list_2
#masterGraph.subgraph()

"""
#######

*def plot_graph_original_position(graph):

This function plot a graph with the vertex at the original position (x,y)

The function takes one parameter:
    * networkx graph: A graph that each node has as name the position as tuple (x,y)

Return the plot with matplotlib


If install in system grpahviz can plot better the graph
nx.draw_graphviz(nameofGraph, 'nameofProgram'(Optional))
nx.draw_graphviz(ng,'circo') #Plot nice


To plot graph with vertex in the original position:
First create a dict with nodes/vertex as keys and position as values
then pass it as parameter after the graph that you want to plot
"""
def plot_graph_original_position(graph):
    d={ (x,y):(x,2048-y) for (x,y) in graph.nodes()}
    #d={ (x,y):(x,y) for (x,y) in graph.nodes()}
    colors=list()
    for v in graph.nodes_iter():
        if 'reference' in graph.node[v]:
            colors.append('r')
        elif graph.node[v]['valid_cells'].__len__()!=0:
            colors.append('b')
        elif graph.node[v]['valid_cells'].__len__()==0:
            colors.append('g')
    nx.draw_networkx(graph,d,node_size=100,node_color=colors,edge_color='y')

def plot_graph_original_position_under_image(graph):
    #d={ (x,y):(x,2048-y) for (x,y) in graph.nodes()}
    d={ (x,y):(x,y) for (x,y) in graph.nodes()}
    colors=list()
    for v in graph.nodes_iter():
        if 'reference' in graph.node[v]:
            colors.append('r')
        elif graph.node[v]['valid_cells'].__len__()!=0:
            colors.append('b')
        elif graph.node[v]['valid_cells'].__len__()==0:
            colors.append('g')
    nx.draw_networkx(graph,d,node_size=100,node_color=colors,edge_color='y')



if __name__ == "__main__":
    import pickle
    import operator
    #(600,549)
    #from mpi4py import MPI
    """
    This file has 5 function:

        *def build_masterGraph_imageX_eachDiagram(image: int):
            * Parameter(1):: int: number, that means the number of a image
            * Return list([networkx grpah,networkx grpah...networkx grpah]: The graph list

        *def save_smallGraph_Information(list_of_graphs: list([nx.graph,nx.graph..nxgraph]) ,image: int):
            * Parameter(2)::
                * list([graph,graph..graph]): A list of graphs of one image,
                * int: The number of the image of the list of graph

        *def get_small_graph(masterGraph: nx.Graph ,vertex: tuple):
            * Parameter(2)::
                * networkx graph: A master Graph / big graph
                * Tuple: (x,y), A vertex that is contained in the masterGraph
            * Return networkx grap: the smallGraph

        *def plot_graph_original_position(graph: nx.rRaph):
            * Parameter(1): * networkx graph: A graph that each node has as name the position as tuple (x,y)
            * Return the plot with matplotlib

        *def equals(smallGraph1: nx.Graph,smallGraph2: nx.Graph):
            * Parameter(2)::
                * nx.Graph,nx.Graph
            * Return tuple(Bool, list(),list())
    """
    diagrams=['1.00', '1.25', '1.50', '1.75', '2.00', '2.25', '2.50', '2.75', '3.00', '3.25', '3.50', '3.75', '4.00', '4.25', '4.50', '4.75', '5.00', '5.25', '5.50', '5.75', '6.00']

    def build_all_graphs():
        from mpi4py import MPI
        COMM=MPI.COMM_WORLD
        #SIZE=COMM.Get_size() #Get number of proceses
        RANK=COMM.Get_rank() #Get id of own process

        if RANK==3:
            for image in range(3,5):#5
                print('RANK:',RANK,'Image:',image)
                masterGraphs_list= build_masterGraph_imageX_eachDiagram(image)
                pickle.dump(masterGraphs_list, open('/bin/master_graphs_image_'+str(image)+'.p','wb'))
                print('RANK:',RANK,'Image:',image,'Finalizada')
                masterGraphs_list.clear()
            MPI.Finalize()
        if RANK==1:
            for image in range(8,10):#10
                print('RANK:',RANK,'Image:',image)
                masterGraphs_list= build_masterGraph_imageX_eachDiagram(image)
                pickle.dump(masterGraphs_list, open('/bin/master_graphs_image_'+str(image)+'.p','wb'))
                print('RANK:',RANK,'Image:',image,'Finalizada')
                masterGraphs_list.clear()
            MPI.Finalize()
        if RANK==2:
            for image in range(13,15):#15
                print('RANK:',RANK,'Image:',image)
                masterGraphs_list= build_masterGraph_imageX_eachDiagram(image)
                pickle.dump(masterGraphs_list, open('/bin/master_graphs_image_'+str(image)+'.p','wb'))
                print('RANK:',RANK,'Image:',image,'Finalizada')
                masterGraphs_list.clear()
            MPI.Finalize()
        if RANK==0:
            for image in range(19,21):#21
                print('RANK:',RANK,'Image:',image)
                masterGraphs_list= build_masterGraph_imageX_eachDiagram(image)
                pickle.dump(masterGraphs_list, open('/bin/master_graphs_image_'+str(image)+'.p','wb'))
                masterGraphs_list.clear()
                print('RANK:',RANK,'Image:',image,'Finalizada')
        print('RANK:',RANK,'Barrera init')
        COMM.Barrier()
        print('RANK:',RANK,'Barrera end')
        MPI.Finalize()

    def extract_all_small_graphs():
        images_data_small_graphs=list() #Each index one image, all small graphs datas
        types_small_graphs=set() #All different types of small graph

        for i,image in enumerate(range(1,21)):
            print('//Image ',image)
            #masterGraphs_list= build_masterGraph_imageX_eachDiagram(image)
            masterGraphs_list=pickle.load( open( '/bin/master_graphs_image_'+str(image)+'.p', 'rb' ) )

            all_dict_small_graph_imageX=list()

            for  d,masterGraph_diagram in enumerate(masterGraphs_list):
                print('Diagram ',diagrams[d])
            #save_smallGraph_Information(masterGraphs_list,1)
                """This work fine,
                    Get all smallGraph of a Graph and them classificate it
                    into a dict, where key is the unique id to each type
                    and values are a list with all vertex of this type/key to
                    build all the smallGraph of this type"""
                type_of_smallGraphs=dict()
                for n in masterGraph_diagram.nodes_iter():# if masterGraph_diagram.node[n]['valid']: #Over each node
                    if masterGraph_diagram.node[n]['valid_cells'].__len__()!=0:
                        sm=get_small_graph(masterGraph_diagram,n) #Get small Graph

                        sm_neighbours_number_list=list()
                        for node_sm in sm.nodes_iter():
                            neighbours_number= len(sm.neighbors(node_sm))
                            sm_neighbours_number_list.append(neighbours_number)
                        sm_neighbours_number_list.sort()
                        unique_id_sm=tuple(sm_neighbours_number_list)
                        if unique_id_sm in type_of_smallGraphs:
                            type_of_smallGraphs[unique_id_sm].add(n)
                        else:
                            types_small_graphs.add(unique_id_sm)
                            type_of_smallGraphs[unique_id_sm]=set([n])


                all_dict_small_graph_imageX.append(type_of_smallGraphs)

            masterGraphs_list=None
            images_data_small_graphs.append(all_dict_small_graph_imageX)

        types_small_graphs=list(types_small_graphs)
        pickle.dump(images_data_small_graphs,open( '/bin/image_data.p', 'wb' ) )
        pickle.dump(types_small_graphs,open( '/bin/types_small_graphs.p', 'wb' ) )

    def count_types_of_small_graphs_at_each_diagram():
        images_data=pickle.load( open( '/bin/image_data.p', 'rb' ) )
        types_small_graphs=pickle.load(open('/bin/types_small_graphs.p','rb'))

        all_dict_small_graph_all_diagrams_count=[dict()]*21 #1 per diagram, save all data of each diagram of each image in one dict. One dict per Diagram

        for d in range(21): #By diagrams
            for l_image in images_data: #By each image
                n_d=all_dict_small_graph_all_diagrams_count[d].copy()
                for k,v in l_image[d].items():
                    if types_small_graphs.index(k) in n_d:
                        n_d[types_small_graphs.index(k)]+=len(v)
                    else:
                        n_d[types_small_graphs.index(k)]=len(v)


                all_dict_small_graph_all_diagrams_count[d]=n_d

        return all_dict_small_graph_all_diagrams_count


    masterGraphs_list= masterGraphs_list=pickle.load( open( 'master_graphs_image_1.p', 'rb' ) )
    def plot_small_graph_by_type(types, masterGraphs_list,types_small_graphs):
        #masterGraphs_list= build_masterGraph_imageX_eachDiagram(1)
        r=masterGraphs_list[0]
        for g in r.nodes_iter():
            sm=get_small_graph(masterGraphs_list[0],g)
            b,l1,l2=equals(sm,sm)
            if masterGraphs_list[0].node[g]['valid_cells'].__len__()!=0 and l1==list(types_small_graphs[types]):
                plot_graph_original_position(sm)
                break
        plt.title('Type '+str(types)+': '+str(types_small_graphs[types]))
        plt.savefig('small_graph_'+str(types)+'.png')
        plt.close()

    def plot_small_graph_by_type_under_image(types, masterGraphs_list,types_small_graphs):
        #masterGraphs_list= build_masterGraph_imageX_eachDiagram(1)
        r=masterGraphs_list[0]
        for g in r.nodes_iter():
            sm=get_small_graph(masterGraphs_list[0],g)
            b,l1,l2=equals(sm,sm)
            if masterGraphs_list[0].node[g]['valid_cells'].__len__()!=0 and l1==list(types_small_graphs[types]):
                plot_graph_original_position_under_image(sm)
                break
        plt.title('Type '+str(types)+': '+str(types_small_graphs[types]))

    def get_all_small_graphs(masterGraph):
        res=list()
        for node in masterGraph.nodes_iter():
            if masterGraph.node[node]['valid_cells'].__len__()!=0:
                sm=get_small_graph(masterGraph,node)
                res.append(sm)
        return res


    def plot_graph_bar(all_dict_small_graph_all_diagrams_count,port):
        for i,diag in enumerate(all_dict_small_graph_all_diagrams_count):
            print('Plot ', diagrams[i])
            d=sorted(diag.items(),key=operator.itemgetter(1),reverse=True)
            num_total=sum([y for x,y in d])
            percent=[(x,(y*100)/num_total) for x,y in d if (y*100)/num_total>=port]
            d=percent

            #d=np.array([(k,v) for k,v in diag.items()])
            d=np.array(d)

            plt.grid( linestyle='-',linewidth=0.4)
            rects=plt.bar(np.arange(start=1,stop=(d[:,1].size*2)+1,step=2),d[:,1])
            plt.yticks(np.arange(0,105,5))
            plt.xticks(np.arange(start=1,stop=(d[:,0].size*2)+1,step=2)+0.4, d[:,0].astype(np.int0),fontsize='small')
            plt.xlabel('Types of Small Graphs: Diagram '+str(diagrams[i]),fontsize=12)
            plt.title('Over '+str(port)+'% Distribution of Small Graphs: Diagram '+str(diagrams[i]))
            #plt.legend(str([str(x)+': '+format(y,'.5g')+'\n' for x,y in d]), fontsize='small')
            #plt.legend(loc='center left', bbox_to_anchor=(1.0, 0.5))


            # attach some text labels
            for rect in rects:
                height = rect.get_height()
                plt.text(rect.get_x()+rect.get_width()/2., 1.0*height, format(height,'.3g')+'%',ha='center', va='bottom', fontsize=9.0)
            #plt.show()
            plt.savefig('Diagram_'+str(diagrams[i])+'_'+str(port)+'%.png')#dpi=600.0)
            plt.close()

