Call:
coxph(formula = Surv(Class) ~ Relation_Neighbour_Ratio_Axis + 
    Clustering_Coefficient + Relation_Neighbour_Angle + Area_Descriptor + 
    Relation_Neighbour_Major_Axis + Convex_Hull + Betweenness + 
    Relation_Neighbour_Area + Area + Area_Normalized + Average_NoN + 
    angle + Ratio_axis + Relation_Neighbour_Convex_Hull + Eccentricity + 
    Ratio_Descriptor + Minor_axis + Relation_Neighbour_Minor_Axis + 
    Major_axis + Strength + Centroid_Descriptor, data = d)

  n= 1320, number of events= 1320 

                                   coef exp(coef) se(coef)      z Pr(>|z|)    
Relation_Neighbour_Ratio_Axis   1.89985   6.68488  0.48060  3.953 7.72e-05 ***
Clustering_Coefficient          0.01627   1.01640  0.42255  0.039 0.969288    
Relation_Neighbour_Angle       -0.49149   0.61171  0.86073 -0.571 0.567988    
Area_Descriptor                 0.06926   1.07171  0.17876  0.387 0.698439    
Relation_Neighbour_Major_Axis  -1.52428   0.21778  0.71779 -2.124 0.033706 *  
Convex_Hull                     1.79349   6.01041  0.92819  1.932 0.053329 .  
Betweenness                     0.42296   1.52648  0.20591  2.054 0.039968 *  
Relation_Neighbour_Area         1.67559   5.34196  1.02538  1.634 0.102234    
Area                            3.82972  46.04963  1.83037  2.092 0.036410 *  
Area_Normalized                 4.39974  81.42974  1.82146  2.416 0.015713 *  
Average_NoN                    -0.72814   0.48281  0.21131 -3.446 0.000569 ***
angle                          -0.06518   0.93690  0.09428 -0.691 0.489353    
Ratio_axis                     -2.43172   0.08789  1.19970 -2.027 0.042669 *  
Relation_Neighbour_Convex_Hull -1.34016   0.26180  0.88110 -1.521 0.128259    
Eccentricity                    0.17586   1.19227  0.19579  0.898 0.369081    
Ratio_Descriptor                0.16936   1.18455  0.29670  0.571 0.568124    
Minor_axis                     -1.06286   0.34547  2.13943 -0.497 0.619332    
Relation_Neighbour_Minor_Axis  -2.31971   0.09830  0.89011 -2.606 0.009158 ** 
Major_axis                     -1.88659   0.15159  1.90890 -0.988 0.323000    
Strength                       -4.08451   0.01683  1.00096 -4.081 4.49e-05 ***
Centroid_Descriptor            -0.40504   0.66695  0.18211 -2.224 0.026137 *  
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

                               exp(coef) exp(-coef) lower .95 upper .95
Relation_Neighbour_Ratio_Axis    6.68488    0.14959  2.606166   17.1469
Clustering_Coefficient           1.01640    0.98386  0.444006    2.3267
Relation_Neighbour_Angle         0.61171    1.63475  0.113213    3.3052
Area_Descriptor                  1.07171    0.93309  0.754944    1.5214
Relation_Neighbour_Major_Axis    0.21778    4.59186  0.053337    0.8892
Convex_Hull                      6.01041    0.16638  0.974605   37.0664
Betweenness                      1.52648    0.65510  1.019569    2.2854
Relation_Neighbour_Area          5.34196    0.18720  0.715971   39.8572
Area                            46.04963    0.02172  1.274120 1664.3404
Area_Normalized                 81.42974    0.01228  2.292755 2892.0674
Average_NoN                      0.48281    2.07122  0.319084    0.7305
angle                            0.93690    1.06735  0.778819    1.1271
Ratio_axis                       0.08789   11.37841  0.008370    0.9228
Relation_Neighbour_Convex_Hull   0.26180    3.81964  0.046557    1.4722
Eccentricity                     1.19227    0.83874  0.812303    1.7500
Ratio_Descriptor                 1.18455    0.84421  0.662222    2.1188
Minor_axis                       0.34547    2.89465  0.005216   22.8818
Relation_Neighbour_Minor_Axis    0.09830   10.17276  0.017175    0.5626
Major_axis                       0.15159    6.59681  0.003596    6.3903
Strength                         0.01683   59.41286  0.002366    0.1197
Centroid_Descriptor              0.66695    1.49937  0.466746    0.9530

Concordance= 0.759  (se = 0.025 )
Rsquare= 0.091   (max possible= 1 )
Likelihood ratio test= 126.7  on 21 df,   p=0
Wald test            = 118.5  on 21 df,   p=1.332e-15
Score (logrank) test = 119.1  on 21 df,   p=1.11e-15

