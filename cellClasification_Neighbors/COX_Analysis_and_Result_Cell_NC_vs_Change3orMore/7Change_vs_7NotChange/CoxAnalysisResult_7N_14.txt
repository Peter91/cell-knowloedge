Call:
coxph(formula = Surv(Class) ~ Relation_Neighbour_Ratio_Axis + 
    Clustering_Coefficient + Relation_Neighbour_Angle + Area_Descriptor + 
    Relation_Neighbour_Major_Axis + Convex_Hull + Betweenness + 
    Relation_Neighbour_Area + Area + Area_Normalized + Average_NoN + 
    angle + Ratio_axis + Relation_Neighbour_Convex_Hull + Eccentricity + 
    Ratio_Descriptor + Minor_axis + Relation_Neighbour_Minor_Axis + 
    Major_axis + Strength + Centroid_Descriptor, data = d)

  n= 1354, number of events= 1354 

                                     coef  exp(coef)   se(coef)      z Pr(>|z|)    
Relation_Neighbour_Ratio_Axis   1.590e+00  4.903e+00  5.078e-01  3.131 0.001744 ** 
Clustering_Coefficient         -7.150e-02  9.310e-01  4.450e-01 -0.161 0.872336    
Relation_Neighbour_Angle       -8.788e-01  4.153e-01  1.002e+00 -0.877 0.380350    
Area_Descriptor                -5.238e-01  5.923e-01  2.723e-01 -1.924 0.054414 .  
Relation_Neighbour_Major_Axis  -4.222e-01  6.556e-01  7.265e-01 -0.581 0.561086    
Convex_Hull                     1.091e+00  2.977e+00  7.310e-01  1.492 0.135639    
Betweenness                     8.743e-01  2.397e+00  2.688e-01  3.252 0.001146 ** 
Relation_Neighbour_Area         1.939e+00  6.953e+00  9.750e-01  1.989 0.046709 *  
Area                            6.665e+00  7.842e+02  1.834e+00  3.634 0.000279 ***
Area_Normalized                 7.476e+00  1.765e+03  1.719e+00  4.350 1.36e-05 ***
Average_NoN                    -1.042e+00  3.528e-01  1.865e-01 -5.588 2.30e-08 ***
angle                          -2.883e-02  9.716e-01  9.672e-02 -0.298 0.765686    
Ratio_axis                     -3.235e-01  7.236e-01  9.722e-01 -0.333 0.739354    
Relation_Neighbour_Convex_Hull -4.421e-01  6.427e-01  6.767e-01 -0.653 0.513515    
Eccentricity                    3.020e-01  1.353e+00  2.173e-01  1.390 0.164619    
Ratio_Descriptor                1.818e+00  6.162e+00  8.404e-01  2.164 0.030479 *  
Minor_axis                     -3.955e+00  1.916e-02  1.679e+00 -2.356 0.018477 *  
Relation_Neighbour_Minor_Axis  -2.016e+00  1.332e-01  8.866e-01 -2.273 0.023007 *  
Major_axis                     -8.762e+00  1.565e-04  2.148e+00 -4.078 4.53e-05 ***
Strength                       -1.296e+00  2.736e-01  9.463e-01 -1.369 0.170860    
Centroid_Descriptor            -2.374e-02  9.765e-01  1.874e-01 -0.127 0.899212    
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

                               exp(coef) exp(-coef) lower .95 upper .95
Relation_Neighbour_Ratio_Axis  4.903e+00  2.040e-01 1.812e+00 1.327e+01
Clustering_Coefficient         9.310e-01  1.074e+00 3.892e-01 2.227e+00
Relation_Neighbour_Angle       4.153e-01  2.408e+00 5.830e-02 2.958e+00
Area_Descriptor                5.923e-01  1.688e+00 3.473e-01 1.010e+00
Relation_Neighbour_Major_Axis  6.556e-01  1.525e+00 1.579e-01 2.723e+00
Convex_Hull                    2.977e+00  3.360e-01 7.104e-01 1.247e+01
Betweenness                    2.397e+00  4.172e-01 1.415e+00 4.060e+00
Relation_Neighbour_Area        6.953e+00  1.438e-01 1.029e+00 4.699e+01
Area                           7.842e+02  1.275e-03 2.155e+01 2.853e+04
Area_Normalized                1.765e+03  5.664e-04 6.080e+01 5.126e+04
Average_NoN                    3.528e-01  2.835e+00 2.448e-01 5.084e-01
angle                          9.716e-01  1.029e+00 8.038e-01 1.174e+00
Ratio_axis                     7.236e-01  1.382e+00 1.076e-01 4.865e+00
Relation_Neighbour_Convex_Hull 6.427e-01  1.556e+00 1.706e-01 2.421e+00
Eccentricity                   1.353e+00  7.394e-01 8.835e-01 2.071e+00
Ratio_Descriptor               6.162e+00  1.623e-01 1.187e+00 3.199e+01
Minor_axis                     1.916e-02  5.218e+01 7.139e-04 5.145e-01
Relation_Neighbour_Minor_Axis  1.332e-01  7.505e+00 2.344e-02 7.574e-01
Major_axis                     1.565e-04  6.389e+03 2.322e-06 1.055e-02
Strength                       2.736e-01  3.654e+00 4.282e-02 1.749e+00
Centroid_Descriptor            9.765e-01  1.024e+00 6.763e-01 1.410e+00

Concordance= 0.817  (se = 0.025 )
Rsquare= 0.142   (max possible= 1 )
Likelihood ratio test= 206.7  on 21 df,   p=0
Wald test            = 194.6  on 21 df,   p=0
Score (logrank) test = 195.7  on 21 df,   p=0

