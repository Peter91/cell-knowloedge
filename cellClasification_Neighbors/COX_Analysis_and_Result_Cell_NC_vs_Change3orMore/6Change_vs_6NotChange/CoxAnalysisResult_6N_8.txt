Call:
coxph(formula = Surv(Class) ~ Relation_Neighbour_Ratio_Axis + 
    Clustering_Coefficient + Relation_Neighbour_Angle + Area_Descriptor + 
    Relation_Neighbour_Major_Axis + Convex_Hull + Betweenness + 
    Relation_Neighbour_Area + Area + Average_NoN + angle + Ratio_axis + 
    Relation_Neighbour_Convex_Hull + Eccentricity + Ratio_Descriptor + 
    Minor_axis + Relation_Neighbour_Minor_Axis + Major_axis + 
    Strength + Centroid_Descriptor, data = d)

  n= 1630, number of events= 1630 

                                   coef exp(coef) se(coef)      z Pr(>|z|)    
Relation_Neighbour_Ratio_Axis   2.26614   9.64214  0.47310  4.790 1.67e-06 ***
Clustering_Coefficient         -0.16842   0.84500  0.44399 -0.379 0.704434    
Relation_Neighbour_Angle        0.51405   1.67205  0.89613  0.574 0.566218    
Area_Descriptor                -0.15355   0.85766  0.18123 -0.847 0.396849    
Relation_Neighbour_Major_Axis  -1.15362   0.31549  0.66711 -1.729 0.083760 .  
Convex_Hull                     1.47933   4.39001  0.84798  1.745 0.081067 .  
Betweenness                     0.58162   1.78894  0.23101  2.518 0.011813 *  
Relation_Neighbour_Area         2.71729  15.13922  0.85046  3.195 0.001398 ** 
Area                            1.79249   6.00436  1.74574  1.027 0.304526    
Average_NoN                    -1.12998   0.32304  0.19872 -5.686 1.30e-08 ***
angle                          -0.13519   0.87355  0.08761 -1.543 0.122806    
Ratio_axis                     -3.30874   0.03656  0.91088 -3.632 0.000281 ***
Relation_Neighbour_Convex_Hull -0.70071   0.49623  0.84207 -0.832 0.405336    
Eccentricity                    0.43944   1.55184  0.18457  2.381 0.017270 *  
Ratio_Descriptor                0.44045   1.55341  0.35320  1.247 0.212388    
Minor_axis                      0.93676   2.55171  1.25391  0.747 0.455017    
Relation_Neighbour_Minor_Axis  -2.79258   0.06126  0.85564 -3.264 0.001100 ** 
Major_axis                      0.24401   1.27635  1.35239  0.180 0.856818    
Strength                       -2.31838   0.09843  0.98828 -2.346 0.018982 *  
Centroid_Descriptor             0.03664   1.03732  0.17366  0.211 0.832893    
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

                               exp(coef) exp(-coef) lower .95 upper .95
Relation_Neighbour_Ratio_Axis    9.64214    0.10371  3.814771   24.3713
Clustering_Coefficient           0.84500    1.18344  0.353941    2.0173
Relation_Neighbour_Angle         1.67205    0.59807  0.288708    9.6836
Area_Descriptor                  0.85766    1.16596  0.601246    1.2234
Relation_Neighbour_Major_Axis    0.31549    3.16964  0.085338    1.1664
Convex_Hull                      4.39001    0.22779  0.833031   23.1351
Betweenness                      1.78894    0.55899  1.137512    2.8134
Relation_Neighbour_Area         15.13922    0.06605  2.858821   80.1715
Area                             6.00436    0.16655  0.196105  183.8420
Average_NoN                      0.32304    3.09558  0.218832    0.4769
angle                            0.87355    1.14476  0.735718    1.0372
Ratio_axis                       0.03656   27.35074  0.006133    0.2180
Relation_Neighbour_Convex_Hull   0.49623    2.01519  0.095260    2.5850
Eccentricity                     1.55184    0.64439  1.080793    2.2282
Ratio_Descriptor                 1.55341    0.64375  0.777392    3.1041
Minor_axis                       2.55171    0.39189  0.218527   29.7960
Relation_Neighbour_Minor_Axis    0.06126   16.32312  0.011452    0.3277
Major_axis                       1.27635    0.78348  0.090118   18.0771
Strength                         0.09843   10.15923  0.014188    0.6829
Centroid_Descriptor              1.03732    0.96402  0.738065    1.4579

Concordance= 0.782  (se = 0.023 )
Rsquare= 0.106   (max possible= 1 )
Likelihood ratio test= 183  on 20 df,   p=0
Wald test            = 170.3  on 20 df,   p=0
Score (logrank) test = 170.4  on 20 df,   p=0

