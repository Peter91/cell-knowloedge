
Cox Analysys, Cells never change 6 neighbours, Class By Area treshold
Without Mayor/Minor axis, Relation_Neighbour_Mayor/Minor Axis, Area_Norm

-----------------
Treshold: 10%
                                     coef  exp(coef)   se(coef)       z Pr(>|z|)      
Area_Descriptor                -1.031e+00  3.566e-01  1.433e-01  -7.198 6.13e-13 ***
Convex_Hull                     1.080e+00  2.944e+00  4.916e-01   2.196 0.028086 *    
Relation_Neighbour_Area         3.405e+00  3.012e+01  1.455e-01  23.398  < 2e-16 ***
Area                            1.078e+01  4.822e+04  4.124e-01  26.148  < 2e-16 ***
Average_NoN                     1.163e+00  3.199e+00  1.112e-01  10.459  < 2e-16 ***   
Ratio_axis                      1.751e+00  5.758e+00  2.598e-01   6.739 1.60e-11 ***
Relation_Neighbour_Convex_Hull -1.648e+00  1.924e-01  4.923e-01  -3.348 0.000815 ***
Ratio_Descriptor               -1.127e+00  3.240e-01  2.344e-01  -4.808 1.52e-06 ***
Strength                       -5.833e+00  2.929e-03  3.761e-01 -15.510  < 2e-16 ***
Centroid_Descriptor             2.702e-01  1.310e+00  8.866e-02   3.048 0.002305 ** 

------------------

Treshold: 15%
                                     coef  exp(coef)   se(coef)       z Pr(>|z|)       
Area_Descriptor                -4.946e-01  6.098e-01  1.469e-01  -3.367 0.000759 ***  
Relation_Neighbour_Area         2.942e+00  1.895e+01  1.417e-01  20.757  < 2e-16 ***
Area                            1.019e+01  2.672e+04  4.289e-01  23.768  < 2e-16 ***
Average_NoN                     1.045e+00  2.844e+00  1.108e-01   9.429  < 2e-16 ***   
Ratio_axis                      1.426e+00  4.163e+00  2.599e-01   5.487 4.09e-08 ***
Relation_Neighbour_Convex_Hull -1.226e+00  2.934e-01  4.836e-01  -2.536 0.011223 *     
Ratio_Descriptor               -1.231e+00  2.921e-01  2.378e-01  -5.174 2.29e-07 ***
Strength                       -5.269e+00  5.146e-03  3.833e-01 -13.747  < 2e-16 ***
Centroid_Descriptor             2.023e-01  1.224e+00  8.922e-02   2.267 0.023399 * 

---------------------

Treshold: 20%
                                     coef  exp(coef)   se(coef)      z Pr(>|z|)      
Area_Descriptor                  -1.06058    0.34626    0.15906 -6.668 2.59e-11 ***    
Relation_Neighbour_Area           2.39362   10.95312    0.14014 17.081  < 2e-16 ***
Area                              7.15363 1278.74287    0.47186 15.161  < 2e-16 ***  
Ratio_axis                        0.96065    2.61339    0.25952  3.702 0.000214 *** 
Ratio_Descriptor                 -1.06643    0.34423    0.23252 -4.586 4.51e-06 ***
Strength                         -3.37735    0.03414    0.41027 -8.232 2.22e-16 ***
Centroid_Descriptor               0.25439    1.28968    0.08900  2.858 0.004257 ** 