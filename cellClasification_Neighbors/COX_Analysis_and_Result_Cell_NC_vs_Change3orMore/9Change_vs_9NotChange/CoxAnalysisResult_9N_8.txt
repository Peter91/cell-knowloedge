Call:
coxph(formula = Surv(Class) ~ Relation_Neighbour_Ratio_Axis + 
    Clustering_Coefficient + Relation_Neighbour_Angle + Area_Descriptor + 
    Relation_Neighbour_Major_Axis + Convex_Hull + Betweenness + 
    Relation_Neighbour_Area + Area + Area_Normalized + Average_NoN + 
    angle + Ratio_axis + Relation_Neighbour_Convex_Hull + Eccentricity + 
    Ratio_Descriptor + Minor_axis + Relation_Neighbour_Minor_Axis + 
    Major_axis + Strength + Centroid_Descriptor, data = d)

  n= 26, number of events= 26 

                                     coef  exp(coef)   se(coef)      z Pr(>|z|)  
Relation_Neighbour_Ratio_Axis  -4.065e+00  1.716e-02  1.348e+01 -0.301   0.7630  
Clustering_Coefficient          9.134e+00  9.269e+03  3.020e+01  0.302   0.7623  
Relation_Neighbour_Angle        2.193e+00  8.964e+00  3.769e+00  0.582   0.5606  
Area_Descriptor                 2.411e+00  1.114e+01  3.991e+00  0.604   0.5458  
Relation_Neighbour_Major_Axis   1.407e+01  1.293e+06  1.372e+01  1.025   0.3052  
Convex_Hull                    -4.020e+00  1.796e-02  4.704e+00 -0.854   0.3929  
Betweenness                    -7.541e-01  4.704e-01  3.002e+00 -0.251   0.8016  
Relation_Neighbour_Area        -6.087e-01  5.440e-01  2.477e+01 -0.025   0.9804  
Area                            5.589e+01  1.867e+24  3.013e+01  1.855   0.0636 .
Area_Normalized                 1.604e+01  9.293e+06  1.682e+01  0.954   0.3401  
Average_NoN                    -3.137e+00  4.343e-02  2.460e+00 -1.275   0.2024  
angle                          -1.749e+00  1.740e-01  2.505e+00 -0.698   0.4852  
Ratio_axis                     -2.896e+01  2.654e-13  1.657e+01 -1.748   0.0805 .
Relation_Neighbour_Convex_Hull  4.595e+00  9.900e+01  2.850e+00  1.612   0.1069  
Eccentricity                   -2.297e+00  1.005e-01  3.251e+00 -0.707   0.4798  
Ratio_Descriptor                7.918e+00  2.746e+03  3.507e+00  2.257   0.0240 *
Minor_axis                     -8.511e+01  1.090e-37  4.861e+01 -1.751   0.0799 .
Relation_Neighbour_Minor_Axis   1.723e+00  5.601e+00  2.742e+01  0.063   0.9499  
Major_axis                     -4.344e+01  1.367e-19  3.163e+01 -1.373   0.1696  
Strength                        2.144e+01  2.058e+09  1.724e+01  1.244   0.2137  
Centroid_Descriptor             4.748e+00  1.153e+02  2.405e+00  1.974   0.0484 *
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

                               exp(coef) exp(-coef) lower .95 upper .95
Relation_Neighbour_Ratio_Axis  1.716e-02  5.826e+01 5.723e-14 5.147e+09
Clustering_Coefficient         9.269e+03  1.079e-04 1.827e-22 4.702e+29
Relation_Neighbour_Angle       8.964e+00  1.116e-01 5.554e-03 1.447e+04
Area_Descriptor                1.114e+01  8.976e-02 4.465e-03 2.780e+04
Relation_Neighbour_Major_Axis  1.293e+06  7.732e-07 2.686e-06 6.229e+17
Convex_Hull                    1.796e-02  5.568e+01 1.778e-06 1.814e+02
Betweenness                    4.704e-01  2.126e+00 1.311e-03 1.689e+02
Relation_Neighbour_Area        5.440e-01  1.838e+00 4.501e-22 6.575e+20
Area                           1.867e+24  5.356e-25 4.201e-02 8.298e+49
Area_Normalized                9.293e+06  1.076e-07 4.494e-08 1.922e+21
Average_NoN                    4.343e-02  2.302e+01 3.497e-04 5.395e+00
angle                          1.740e-01  5.747e+00 1.283e-03 2.360e+01
Ratio_axis                     2.654e-13  3.768e+12 2.098e-27 3.358e+01
Relation_Neighbour_Convex_Hull 9.900e+01  1.010e-02 3.711e-01 2.641e+04
Eccentricity                   1.005e-01  9.947e+00 1.717e-04 5.887e+01
Ratio_Descriptor               2.746e+03  3.642e-04 2.839e+00 2.656e+06
Minor_axis                     1.090e-37  9.170e+36 4.621e-79 2.573e+04
Relation_Neighbour_Minor_Axis  5.601e+00  1.785e-01 2.541e-23 1.235e+24
Major_axis                     1.367e-19  7.315e+18 1.636e-46 1.143e+08
Strength                       2.058e+09  4.860e-10 4.315e-06 9.814e+23
Centroid_Descriptor            1.153e+02  8.671e-03 1.034e+00 1.286e+04

Concordance= 1  (se = 0.179 )
Rsquare= 0.647   (max possible= 0.991 )
Likelihood ratio test= 27.11  on 21 df,   p=0.1674
Wald test            = 13.72  on 21 df,   p=0.8813
Score (logrank) test = 27.89  on 21 df,   p=0.1433

