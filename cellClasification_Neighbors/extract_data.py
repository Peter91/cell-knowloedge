# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import os
import random
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
#from mpi4py import MPI

class count_Cells():

    def __init__(self):
        self.dir=os.getcwd()

        """Next line get neighbour data from Diagram_2.0 to 6.0 as a numpy array, [[6,6,6..]..[]]
        This file is a csv only with Neighbours data"""
        self.f=np.loadtxt(self.dir+'/neighbour_data.csv',dtype='i1',skiprows=2,usecols=np.arange(8,25), delimiter=',')

        """Next line get area data from Diagrama_1.0 to 6.0 as a numpy array, [[6,6,6..]..[]]
        This file is a csv only with Area data"""
        self.area=np.loadtxt(self.dir+'/area_data.csv',dtype='i10',skiprows=2,usecols=np.arange(4,25), delimiter=',')

        """Next line open the file that contains the datas separated in matrix for each propierties"""
        f=np.loadtxt(os.getcwd()+'/Matriz_cell_data_propierties_separated.csv',dtype='S',delimiter=',',ndmin=1)
        self.propierties=[ x.astype(np.str).replace('"','') for x in f[0] if x.__len__()!=0] #Get propierties name

        """Next line, data structure to save datas:
            keys: str (propierties name)
            values: np-ndarray( np.array cells datas, each cell 1 array) [[cell 1 data],[cell 2 data]....]
        """
        self.datas_all_prop=dict()
        init=4
        fin=25
        for x in range(self.propierties.__len__()):
            self.datas_all_prop[self.propierties[x]]=f[2:,init:fin].astype(np.float64)
            init+=26
            fin=init+21

        #self.rows= np.array(k.replace("\n","").split(",") for k in f)
        #self.rows= np.array(f.readlines()[2:],ndmin=2)


    """cell_count_never_change()
    This method return cells that never change from Diagram_2.0 to 6.0
    """
    def cell_count_never_change(self):
        d=map(set,self.f) #Get set of neighbour between 2.0 and 6.0
        res=dict() #Data structure key: str(num neighbour) - value: count cell
        res_1=dict() #Data structure key: int(num neighbour) - value:[list of cell index ]])
        res_2=list() #All cell index in a list
        for i,x in enumerate(d):
            if x.__len__()==1: #We filter by length 1 because is the cell that never change
                c=x.pop()
                if c not in res: #Add to dict if no exist
                    res[c]=1
                    res_1[c]=[i]

                else: #Update the dict
                    val=res[c]+1
                    val_1=res_1[c]+[i]

                    res[c]=val
                    res_1[c]=val_1
                res_2.append(i)
        """Return 2 dict, first cell count, second index of each cell, 3rd list of all index"""
        return res,res_1,res_2

    """cell_change_x_times()
    This method count cell that change number of neighbours x times
    """
    def cell_change_x_times(self,number_of_changes):
        res=dict() #Data structure key: str(num neighbours init) - value: count cell
        res_cell_index=dict() #Data structure key: str(num neighbour) - value:[list of number/index of cell in the excel]]))

        for i,c in enumerate(self.f):
            num_change=0
            for x,y in zip(c[:-1],c[1:]):
                if x!=y: num_change+=1

            if num_change==number_of_changes: #Here filter cell by number_of_changes
                if c[0] not in res: #Add to dict if no exist
                    res[c[0]]=1
                    res_cell_index[c[0]]=[i]
                else: #Update dict
                    val=res[c[0]]+1
                    val_i=res_cell_index[c[0]]+[i]

                    res[c[0]]=val
                    res_cell_index[c[0]]=val_i
        """Return 2 dict: first cell count, second index of each cell"""
        return res,res_cell_index

    """cell_change_all_count()
    This method count all different changes, between 2.0 and 6.0
    """
    def cell_change_all_count(self):
        res=[dict()]*15 #Data structure list[( each index indicate number of neighbours change), dict key: str(num neighbours init) - value: count cell]
        res_cell_index=[dict()]*15 #Data structure list(each index indicate number of neighbours change), dict key: str(num neighbours init) - value:[list of number/index of cell in the excel]
        res_2=list() #All cell index in a list
        for i,c in enumerate(self.f):
            num_change=0
            for x,y in zip(c[:-1],c[1:]):
                if x!=y: num_change+=1
            """Add to res_2(list) index of cells that change 1 or more times"""

            d=res[num_change].copy()
            d_i=res_cell_index[num_change].copy()
            if c[0] not in d:#Add to dict if no exist
                d[c[0]]=1
                d_i[c[0]]=[i]
            else: #Update dict
                val=d[c[0]]+1
                val_i=d_i[c[0]]+[i]

                d[c[0]]=val
                d_i[c[0]]=val_i
            res_2.append(i)

            res[num_change]=d
            res_cell_index[num_change]=d_i
        print(len(res_2))
        """Return 2 dict, 1 list: first cell count, second index of each cell, 3rd list of all index"""
        return res,res_cell_index,res_2


    """cell_change_init_equal_end()
    This method count all different changes filtering cell that init and end neighbours number are equals
    """
    def cell_change_init_equal_end(self):
        res=[dict()]*15 #Data structure list( each index indicate number of neighbours change), dict key: str(num neighbours init) - value: count cell
        res_cell_index=[dict()]*15 #Data structure key: str(num neighbour) - value:[list of number/index of cell in the excel]])
        for i,c in enumerate(self.f):
            num_change=0
            if c[0]==c[-1]: #Filter by cell that at D2.0 and D6.0 has same neighbours number
                for x,y in zip(c[:-1],c[1:]):
                    if x!=y: num_change+=1
                d=res[num_change].copy()
                d_i=res_cell_index[num_change].copy()
                if c[0] not in d:#Add to dict if no exist
                    d[c[0]]=1
                    d_i[c[0]]=[i]
                else: #Update dict
                    val=d[c[0]]+1
                    val_i=d_i[c[0]]+[i]

                    d[c[0]]=val
                    d_i[c[0]]=val_i

                res[num_change]=d
                res_cell_index[num_change]=d_i

        """Return 2 dict: first cell count, second index of each cell"""
        return res,res_cell_index

    """cell_change_init_diff_end()
    This method count all different changes filter cell that init and end are different
    """
    def cell_change_init_diff_end(self):
        res=[dict()]*15 #Data structure list( each index indicate number of neighbours change), dict key: str(num neighbours init) - value: count cell
        res_cell_index=[dict()]*15 #Data structure key: str(num neighbour) - value:[list of number/index of cell in the excel]])

        for i,c in enumerate(self.f):
            num_change=0
            if c[0]!=c[-1]: #Filter by cell  that at D2.0 and D6.0 has different neighbours number
                for x,y in zip(c[:-1],c[1:]):
                    if x!=y: num_change+=1

                d=res[num_change].copy()
                d_i=res_cell_index[num_change].copy()
                if str(c[0])+'-'+str(c[-1]) not in d: #Add to dict if no exist
                    d[str(c[0])+'-'+str(c[-1])]=1 #Key is: str(init_num_neighbour(2.0)-final_num_neighbour(6.0))
                    d_i[str(c[0])+'-'+str(c[-1])]=[i]
                else: #Update dict
                    val=d[str(c[0])+'-'+str(c[-1])]+1
                    val_i=d_i[str(c[0])+'-'+str(c[-1])]+[i]

                    d[str(c[0])+'-'+str(c[-1])]=val
                    d_i[str(c[0])+'-'+str(c[-1])]=val_i

                res[num_change]=d
                res_cell_index[num_change]=d_i

        """Return 2 dict: first cell count, second index of each cell"""
        return res,res_cell_index


    """cell_change_from2.0_to_3.0()
    This method count all different changes between Diagram2.0-2.75
    and them not change from 2.75to6.0
    """
    def cell_change_from2to_275(self):
        res=[dict()]*15 #Data structure list( each index indicate number of neighbours change), dict key: str(num neighbours init) - value: count cell
        res_cell_index=[dict()]*15 #Data structure key: str(num neighbour) - value:[list of number/index of cell in the excel]])

        for i,c in enumerate(self.f):
            num_change=0
            if set(c[3:]).__len__()==1: #Filter by cell that between D2.75 and D6.0 not change number of neighbours
                for x,y in zip(c[:4],c[1:4]): #Count change from D2.0 to D2.75
                    if x!=y: num_change+=1

                d=res[num_change].copy()
                d_i=res_cell_index[num_change].copy()
                if str(c[0])+'-'+str(c[-1]) not in d: #Add to dict if no exist
                    d[str(c[0])+'-'+str(c[-1])]=1 #Key is: str(init_num_neighbour(2.0)-final_num_neighbour(6.0))
                    d_i[str(c[0])+'-'+str(c[-1])]=[i]
                else: #Update dict
                    val=d[str(c[0])+'-'+str(c[-1])]+1
                    val_i=d_i[str(c[0])+'-'+str(c[-1])]+[i]

                    d[str(c[0])+'-'+str(c[-1])]=val
                    d_i[str(c[0])+'-'+str(c[-1])]=val_i

                res[num_change]=d
                res_cell_index[num_change]=d_i

        """Return 2 dict: first cell count, second index of each cell"""
        return res,res_cell_index




def norm(data):
    mx=np.max(data)
    mn=np.min(data)
    res=list()
    for i in data:
        n=(i-mn)/(mx-mn)
        res.append(n)
    return res

class extract_Datas_by_index_to_csv():

    def __init__(self,cell_count):
        self.cell_count=cell_count

    def extractDatas_AllCells_noFilter(self):
        x,i,z= self.cell_count.cell_change_all_count()

        res=list()
        props=self.cell_count.propierties[:]
        for prop in self.cell_count.propierties:
            res_2=list()        #res_6=list()
            if not(prop=='Neighbour' or prop=='Neighbour Descriptor'):
                for index in z: #Iteramos por los indices de las células
                    res_2.append(self.cell_count.datas_all_prop[prop][index][4]) #Nos quedamos con el valor de la Propiedad: prop, de indice: index, y Diagrma 2.0: 4
                res_2_norm=norm(res_2)
                res.append(np.array(res_2_norm))
            else: props.remove(prop)
            #f2_norm.write(prop.replace(' ','_')+','+str(res_2_norm).replace('[','').replace(']','')+'\n')

        f2_norm=open('D2_AllCells_norm.csv', mode='x')
        f2_norm.close()
        np.savetxt('D2_AllCells_norm.csv',np.transpose(res),delimiter=',',header=str(props).replace('[','').replace(']','').replace("'",'')+',Class', comments='')

    def extractDatas_cellChange_no_filter(self):
        x,i,z= self.cell_count.cell_change_all_count()

        res=list()
        props=self.cell_count.propierties
        for prop in self.cell_count.propierties:
            res_2=list()        #res_6=list()
            if not(prop=='Neighbour' or prop=='Neighbour Descriptor'):
                for index in z: #Iteramos por los indices de las células
                    res_2.append(self.cell_count.datas_all_prop[prop][index][4]) #Nos quedamos con el valor de la Propiedad: prop, de indice: index, y Diagrma 2.0: 4
                res_2_norm=norm(res_2)
                res.append(np.array(res_2_norm))
            else: props.remove(prop)
            #f2_norm.write(prop.replace(' ','_')+','+str(res_2_norm).replace('[','').replace(']','')+'\n')

        f2_norm=open('D2_change_All_norm.csv', mode='x')
        f2_norm.close()
        np.savetxt('D2_change_All_norm.csv',np.transpose(res),delimiter=',',header=str(props).replace('[','').replace(']','').replace("'",'')+',Class', comments='')

    def extractDatas_cellChange_no_filter_random(self,cellNumberRandomSelection,filesNumber):
        x,i,z= self.cell_count.cell_change_all_count()

        for times in range(filesNumber):
            index_random=random.sample(z,cellNumberRandomSelection)
            res=list()
            props=self.cell_count.propierties
            for prop in self.cell_count.propierties:
                res_2=list()        #res_6=list()
                if not(prop=='Neighbour' or prop=='Neighbour Descriptor'):
                    for index in index_random: #Iteramos por los indices de las células
                        res_2.append(self.cell_count.datas_all_prop[prop][index][4]) #Nos quedamos con el valor de la Propiedad: prop, de indice: index, y Diagrma 2.0: 4
                    res_2_norm=norm(res_2)
                    res.append(np.array(res_2_norm))
                else: props.remove(prop)
                #f2_norm.write(prop.replace(' ','_')+','+str(res_2_norm).replace('[','').replace(']','')+'\n')

            f2_norm=open('D2_change_All_norm_'+str(times)+'.csv', mode='x')
            f2_norm.close()
            np.savetxt('D2_change_All_norm_'+str(times)+'.csv',np.transpose(res),delimiter=',',header=str(props).replace('[','').replace(']','').replace("'",'')+',Class', comments='')



    def extractDatas_cellChange_can_filter(self,cell_filter):

        x,i,z= self.cell_count.cell_change_all_count()
        #f2=open('D2_change_6.csv', mode='x')
        #f6=open('D6_change_6.csv', mode='x')
        #f6_norm=open('D6_change_6_norm.csv', mode='x')
        props=self.cell_count.propierties[:]
        res=list()
        for prop in self.cell_count.propierties:
            res_2=list()
            #res_6=list()
            if not(prop=='Neighbour' or prop=='Neighbour Descriptor'):
                for d in i[3:]: #Cogemos dict de celulas que cambian 3 o mas veces                ]
                    for neighbour_index,listIndex in d.items(): #Iteramos por las claves, las claves indican los vecinos que tienen esas células en el Diagrama 2.0, y valores de esa clave listIndex: lista(list) de indices(int) de células que tienen neighbour_index vecinos en el Diagrama 2.0
                        if neighbour_index==cell_filter: #Filtramos según el numero de vecinos en Diagrama 2.0
                            for index in listIndex: #Iteramos por valores, indices de cúlulas:[lista de cell index(int)] de la clave neighbour_index
                                res_2.append(self.cell_count.datas_all_prop[prop][index][4]) #Nos quedamos con el valor de la Propiedad: prop, de indice: index, y Diagrma 2.0: 4
                            #res_6.append(val[index][20])
                res_2_norm=norm(res_2)
                res.append(np.array(res_2_norm))
            else: props.remove(prop)
            #res_6_norm=norm(res_6)
            #print('Cambian: ',len(res_2))


        #f2.close()
        #f6.close()
        f2_norm=open('D2_change_'+str(cell_filter)+'_norm.csv', mode='x')
        f2_norm.close()
        np.savetxt('D2_change_'+str(cell_filter)+'_norm.csv',np.transpose(res),delimiter=',',header=str(props).replace('[','').replace(']','').replace("'",'')+',Class', comments='')


    def extractDatas_cellChange_can_filter_random(self,cell_filter,cellNumberRandomSelection,files_number):

        x,i,z= self.cell_count.cell_change_all_count()
        """First we need to get all the index
        and them get a random sample of this"""
        indexs=list()
        for d in i[1:]:#Cogemos dict de celulas que cambian 3 o mas veces
            for neighbour_index,listIndex in d.items():
                if neighbour_index==cell_filter:
                    indexs.extend(listIndex)
        print(indexs)
        for times in range(files_number):
            index_sample=random.sample(indexs,cellNumberRandomSelection)
            res=list()
            props=self.cell_count.propierties[:]

            for prop in self.cell_count.propierties:
                res_2=list()
                #res_6=list()
                if not(prop=='Neighbour' or prop=='Neighbour Descriptor'):
                    for index in index_sample:
                        res_2.append(self.cell_count.datas_all_prop[prop][index][4]) #Nos quedamos con el valor de la Propiedad: prop, de indice: index, y Diagrma 2.0: 4
                                #res_6.append(val[index][20])
                    res_2_norm=norm(res_2)
                    res.append(np.array(res_2_norm))
                else: props.remove(prop)


            f2_norm=open('D2_change_'+str(cell_filter)+'_norm_'+str(times)+'.csv', mode='x')
            f2_norm.close()
            np.savetxt('D2_change_'+str(cell_filter)+'_norm_'+str(times)+'.csv',np.transpose(res),delimiter=',',header=str(props).replace('[','').replace(']','').replace("'",'')+',Class', comments='')

    def extractDatas_cellNeverChange_no_filter(self):
        x,i,z= self.cell_count.cell_count_never_change()

        res=list()
        props=self.cell_count.propierties[:]
        for prop in self.cell_count.propierties:
            val=self.cell_count.datas_all_prop[prop]
            res_2=list()
            if (prop=='Neighbour' or prop=='Neighbour Descriptor'):
                print('hola')
                props.remove(prop)
            else:
                print(prop)
                for index in z: #Iteramos por valores, indices de cúlulas:[lista de cell index(int)] de la clave neighbour_index
                    res_2.append(val[index][4]) #Nos quedamos con el valor de la Propiedad: prop, de indice: index, y Diagrma 2.0: 4
                res_2_norm=norm(res_2)
                res.append(np.array(res_2_norm))

        f2_norm=open('D2_not_change_All_norm.csv', mode='x')
        f2_norm.close()
        np.savetxt('D2_not_change_All_norm.csv', np.transpose(res),delimiter=',',header=str(props).replace('[','').replace(']','').replace("'",'')+',Class', comments='')

    def extractDatas_cellNeverChange_no_filter_random(self,cellNumberRandomSelection,filesNumber):
        x,i,z= self.cell_count.cell_count_never_change()


        for times in range(filesNumber):
            index_random=random.sample(z,cellNumberRandomSelection)
            res=list()
            props=self.cell_count.propierties[:]
            for prop in self.cell_count.propierties:
                val=self.cell_count.datas_all_prop[prop]
                res_2=list()
                if (prop=='Neighbour' or prop=='Neighbour Descriptor'):
                    print('hola')
                    props.remove(prop)
                else:
                    print(prop)
                    for index in index_random: #Iteramos por valores, indices de cúlulas:[lista de cell index(int)] de la clave neighbour_index
                        res_2.append(val[index][4]) #Nos quedamos con el valor de la Propiedad: prop, de indice: index, y Diagrma 2.0: 4
                    res_2_norm=norm(res_2)
                    res.append(np.array(res_2_norm))

            f2_norm=open('D2_not_change_All_'+str(times)+'.csv', mode='x')
            f2_norm.close()
            np.savetxt('D2_not_change_All_'+str(times)+'.csv', np.transpose(res),delimiter=',',header=str(props).replace('[','').replace(']','').replace("'",'')+',Class', comments='')

    def extractDatas_cellNeverChange_can_filter(self,cell_filter):
        x,i,z= self.cell_count.cell_count_never_change()
        #f2=open('D2_not_change_6.csv', mode='x')
        #f6=open('D6_not_change_6.csv', mode='x')
        #f6_norm=open('D6_not_change_6_norm.csv', mode='x')
        res=list()
        props=self.cell_count.propierties[:]
        for prop in self.cell_count.propierties:
            val=self.cell_count.datas_all_prop[prop]
            res_2=list()
            #res_6=list()

            if (prop=='Neighbour' or prop=='Neighbour Descriptor'):
                print(prop)
                props.remove(prop)
            else:
                for neighbour_init,listIndex in i.items():#Iteramos por las claves, las claves indican los vecinos que tienen esas células en el Diagrama 2.0, y valores de esa clave listIndex: lista(list) de indices(int) de células que tienen neighbour_index vecinos en el Diagrama 2.0
                    if neighbour_init==cell_filter:
                        for index in listIndex: #Iteramos por valores, indices de cúlulas:[lista de cell index(int)] de la clave neighbour_index
                            res_2.append(val[index][4]) #Nos quedamos con el valor de la Propiedad: prop, de indice: index, y Diagrma 2.0: 4
                        #res_6.append(val[index][20])
                res_2_norm=norm(res_2)
                res.append(np.array(res_2_norm))

                #res_6_norm=norm(res_6)
                #print('No Cambian: ',len(res_2))

        #f2.close()
        #f6.close()
        f2_norm=open('D2_not_change_'+str(cell_filter)+'_norm.csv', mode='x')
        f2_norm.close()
        np.savetxt('D2_not_change_'+str(cell_filter)+'_norm.csv', np.transpose(res),delimiter=',',header=str(props).replace('[','').replace(']','').replace("'",'')+',Class', comments='')

    def extractDatas_cellNeverChange_can_filter_random(self,cell_filter,cellNumberRandomSelection,files_number):

        x,i,z= self.cell_count.cell_count_never_change()
        #f2=open('D2_not_change_6.csv', mode='x')
        #f6=open('D6_not_change_6.csv', mode='x')
        #f6_norm=open('D6_not_change_6_norm.csv', mode='x')
        for times in range(files_number):
            index_sample=random.sample(i[cell_filter],cellNumberRandomSelection)
            res=list()
            resn=list()

            props=self.cell_count.propierties[:]
            for prop in self.cell_count.propierties:
                val=self.cell_count.datas_all_prop[prop]
                res_2=list()
                #res_6=list()

                if (prop=='Neighbour' or prop=='Neighbour Descriptor'):
                    print(prop)
                    props.remove(prop)
                else:
                    for index in  index_sample:#Iteramos por las claves, las claves indican los vecinos que tienen esas células en el Diagrama 2.0, y valores de esa clave listIndex: lista(list) de indices(int) de células que tienen neighbour_index vecinos en el Diagrama 2.0
                        res_2.append(val[index][4]) #Nos quedamos con el valor de la Propiedad: prop, de indice: index, y Diagrma 2.0: 4
                            #res_6.append(val[index][20])

                    resn.append(np.array(res_2))
                    res_2_norm=norm(res_2)
                    res.append(np.array(res_2_norm))
                    #res_6_norm=norm(res_6)

            f2_norm=open('D2_not_change_'+str(cell_filter)+'_norm_'+str(times)+'.csv', mode='x')
            f2_norm.close()
            np.savetxt('D2_not_change_'+str(cell_filter)+'_norm_'+str(times)+'.csv', np.transpose(res),delimiter=',',header=str(props).replace('[','').replace(']','').replace("'",'')+',Class', comments='')
            #f6_norm.close()


    def extract3Class_cells(self):
        x,i,z= self.cell_count.cell_change_all_count()


        """This metdhon return a data structure which is a list with 3 list in.
        This 3 list difference 3 classes:

            List 1 - Index 0 - Class 1: Cells Change=1/
                                        Cells Not Change=0

            List 2 - Index 1 - Class 2: Number of neighbours at Diagram 2.0 (3,4,5....11)

            List 3 - Index 2 - Class 3: Cells Not Change=-1/
                                        Cell Change 1 or 2 times=0/
                                        Cell Change 3 or more times=1
            """
        res=[[None]*32425,[None]*32425,[None]*32425]

        for changes, d in enumerate(i):  #Cogemos dict de celulas que cambian 3 o mas veces
            for neighbour_index,listIndex in d.items(): #Iteramos por las claves, las claves indican los vecinos que tienen esas células en el Diagrama 2.0, y valores de esa clave listIndex: lista(list) de indices(int) de células que tienen neighbour_index vecinos en el Diagrama 2.0
                for index in listIndex: #Iteramos por valores, indices de cúlulas:[lista de cell index(int)] de la clave neighbour_index
                        if changes==0:
                            res[0][index]=0
                        else:
                            res[0][index]=1

                        res[1][index]=neighbour_index

                        if changes==1 or changes==2:
                            res[2][index]=0#'Change1or2'
                        elif changes>=3:
                            res[2][index]=1#'Change3or+'
                        else:
                            res[2][index]=-1#'NoChange'

        f_classes=open('Cells_Classes.csv', mode='x')
        f_classes.close()
        np.savetxt('Cells_Classes.csv',np.transpose(res),delimiter=',',fmt='%s',header='Class_1,Class_2,Class_3', comments='')



    def extractDatas_All_can_filter_RandomChange_ManyFiles(self,cell_filter,cellNumberRandomSelection,files_number):

        x,i,z= self.cell_count.cell_change_all_count()
        x1,i1,z1= self.cell_count.cell_count_never_change()

        for times in range(files_number):

            props=self.cell_count.propierties[:]
            res=list()

            """Random Selection of Cells that Change"""
            """First we need to get all the index
            and them get a random sample of this"""
            indexs=list()
            for d in i[3:]:#Cogemos dict de celulas que cambian 3 o mas veces
                for neighbour_index,listIndex in d.items():
                    if neighbour_index==cell_filter:
                        indexs.extend(listIndex)
            index_sample=random.sample(indexs,cellNumberRandomSelection)

            for prop in self.cell_count.propierties:
                res_2=list()

                if not(prop=='Neighbour' or prop=='Neighbour Descriptor'):
                    """Cells Change"""

                    for index in index_sample: #Iteramos por valores, indices de cúlulas:[lista de cell index(int)] de la clave neighbour_index
                        res_2.append(self.cell_count.datas_all_prop[prop][index][4]) #Nos quedamos con el valor de la Propiedad: prop, de indice: index, y Diagrma 2.0: 4


                    """Cells Never Change"""
                    for neighbour_init,listIndex in i1.items():#Iteramos por las claves, las claves indican los vecinos que tienen esas células en el Diagrama 2.0, y valores de esa clave listIndex: lista(list) de indices(int) de células que tienen neighbour_index vecinos en el Diagrama 2.0
                        if neighbour_init==cell_filter:
                            for index in listIndex: #Iteramos por valores, indices de cúlulas:[lista de cell index(int)] de la clave neighbour_index
                                res_2.append(self.cell_count.datas_all_prop[prop][index][4]) #Nos quedamos con el valor de la Propiedad: prop, de indice: index, y Diagrma 2.0: 4


                    res_2_norm=norm(res_2)
                    res.append(np.array(res_2_norm))

                else: props.remove(prop)

            res.append([1]*cellNumberRandomSelection+[0]*cellNumberRandomSelection) #To add class column
            props=[p.replace(' ','_') for p in props] #To avoid space at propierties name
            f2_norm=open('D2_All_'+str(cell_filter)+'_norm_'+str(times)+'.csv', mode='x')
            f2_norm.close()
            np.savetxt('D2_All_'+str(cell_filter)+'_norm_'+str(times)+'.csv',np.transpose(res),delimiter=',',header=str(props).replace('[','').replace(']','').replace("'",'')+',Class', comments='')

    def extractDatas_All_can_filter_RandomNoChange_ManyFiles(self,cell_filter,cellNumberRandomSelection,files_number):

        x,i,z= self.cell_count.cell_change_all_count()
        x1,i1,z1= self.cell_count.cell_count_never_change()


        for times in range(files_number):
            props=self.cell_count.propierties[:]
            res=list()

            """Random Selection of Cells that Never Change"""
            index_sample=random.sample(i1[cell_filter],cellNumberRandomSelection)

            for prop in self.cell_count.propierties:
                res_2=list()

                if not(prop=='Neighbour' or prop=='Neighbour Descriptor'):

                    """Cells Change"""
                    for d in i[3:]: #Cogemos dict de celulas que cambian 3 o mas veces                ]
                        for neighbour_index,listIndex in d.items(): #Iteramos por las claves, las claves indican los vecinos que tienen esas células en el Diagrama 2.0, y valores de esa clave listIndex: lista(list) de indices(int) de células que tienen neighbour_index vecinos en el Diagrama 2.0
                            if neighbour_index==cell_filter: #Filtramos según el numero de vecinos en Diagrama 2.0
                                for index in listIndex:
                                    res_2.append(self.cell_count.datas_all_prop[prop][index][4]) #Nos quedamos con el valor de la Propiedad: prop, de indice: index, y Diagrma 2.0: 4


                    """Cells Never Change"""
                    for index in index_sample: #Iteramos por valores, indices de cúlulas:[lista de cell index(int)] de la clave neighbour_index
                        res_2.append(self.cell_count.datas_all_prop[prop][index][4]) #Nos quedamos con el valor de la Propiedad: prop, de indice: index, y Diagrma 2.0: 4


                    res_2_norm=norm(res_2)
                    res.append(np.array(res_2_norm))

                else: props.remove(prop)

            res.append([1]*cellNumberRandomSelection+[0]*cellNumberRandomSelection) #To add class column
            props=[p.replace(' ','_') for p in props] #To avoid space at propierties name
            f2_norm=open('D2_All_'+str(cell_filter)+'_norm_'+str(times)+'.csv', mode='x')
            f2_norm.close()
            np.savetxt('D2_All_'+str(cell_filter)+'_norm_'+str(times)+'.csv',np.transpose(res),delimiter=',',header=str(props).replace('[','').replace(']','').replace("'",'')+',Class', comments='')

    def extractDatas_All_can_filter_ClassByNeighbours_OneFiles(self,cell_filter):

        x,i,z= self.cell_count.cell_change_all_count()
        x1,i1,z1= self.cell_count.cell_count_never_change()

        props=self.cell_count.propierties[:]
        res=list()
        cellChangeNumber=0
        cellNoChangeNumber=0


        for prop in self.cell_count.propierties:
            res_2=list()

            if not(prop=='Neighbour' or prop=='Neighbour Descriptor'):

                """Cells Change"""
                for d in i[1:]: #Cogemos dict de celulas que cambian 3 o mas veces                ]
                    for neighbour_index,listIndex in d.items(): #Iteramos por las claves, las claves indican los vecinos que tienen esas células en el Diagrama 2.0, y valores de esa clave listIndex: lista(list) de indices(int) de células que tienen neighbour_index vecinos en el Diagrama 2.0
                        if neighbour_index==cell_filter: #Filtramos según el numero de vecinos en Diagrama 2.0
                            for index in listIndex:
                                res_2.append(self.cell_count.datas_all_prop[prop][index][4]) #Nos quedamos con el valor de la Propiedad: prop, de indice: index, y Diagrma 2.0: 4
                cellChangeNumber=len(res_2)
                """Cells Never Change"""
                for neighbour_init,listIndex in i1.items():#Iteramos por las claves, las claves indican los vecinos que tienen esas células en el Diagrama 2.0, y valores de esa clave listIndex: lista(list) de indices(int) de células que tienen neighbour_index vecinos en el Diagrama 2.0
                    if neighbour_init==cell_filter:
                        for index in listIndex: #Iteramos por valores, indices de cúlulas:[lista de cell index(int)] de la clave neighbour_index
                            res_2.append(self.cell_count.datas_all_prop[prop][index][4]) #Nos quedamos con el valor de la Propiedad: prop, de indice: index, y Diagrma 2.0: 4

                cellNoChangeNumber=len(res_2)-cellChangeNumber
                res_2_norm=norm(res_2)
                res.append(np.array(res_2_norm))

            else: props.remove(prop)

        res.append(['Change']*cellChangeNumber+['NoChange']*cellNoChangeNumber) #To add class column
        props=[p.replace(' ','_') for p in props] #To avoid space at propierties name
        f2_norm=open('D2_All_'+str(cell_filter)+'_norm_.csv', mode='x')
        f2_norm.close()
        print(len(np.transpose(res)[0]))
        np.savetxt('D2_All_'+str(cell_filter)+'_norm_.csv',np.transpose(res),delimiter=',',header=str(props).replace('[','').replace(']','').replace("'",'')+',Class', comments='',fmt='%s')

    def extractDatas_All_can_filter_3ClassByArea_OneFiles(self,cell_filter,area_treshold):

        x,i,z= self.cell_count.cell_change_all_count()
        x1,i1,z1= self.cell_count.cell_count_never_change()

        props=self.cell_count.propierties[:]
        res=list()
        resNorm=list()
        flagClasses=True
        classesList=list()
        classesListNum=list()
        for prop in self.cell_count.propierties:
            res_2=list()

            if not(prop=='Neighbour' or prop=='Neighbour Descriptor'):

                """Cells Change"""
                for d in i[3:]: #Cogemos dict de celulas que cambian 3 o mas veces                ]
                    for neighbour_index,listIndex in d.items(): #Iteramos por las claves, las claves indican los vecinos que tienen esas células en el Diagrama 2.0, y valores de esa clave listIndex: lista(list) de indices(int) de células que tienen neighbour_index vecinos en el Diagrama 2.0
                        if neighbour_index==cell_filter: #Filtramos según el numero de vecinos en Diagrama 2.0
                            for index in listIndex:
                                res_2.append(self.cell_count.datas_all_prop[prop][index][4]) #Nos quedamos con el valor de la Propiedad: prop, de indice: index, y Diagrma 2.0: 4
                                if flagClasses:
                                    areaUpTen=self.cell_count.area[index][4]+self.cell_count.area[index][4]*area_treshold
                                    areaDownTen=self.cell_count.area[index][4]-self.cell_count.area[index][4]*area_treshold
                                    if self.cell_count.area[index][-1]>areaUpTen:
                                        classesList.append('Increase')
                                        classesListNum.append(1)
                                    elif self.cell_count.area[index][-1]<areaDownTen:
                                        classesList.append('Decrease')
                                        classesListNum.append(-1)
                                    else:
                                        classesList.append('Nothing_Equal')
                                        classesListNum.append(0)



                """Cells Never Change"""
                for neighbour_init,listIndex in i1.items():#Iteramos por las claves, las claves indican los vecinos que tienen esas células en el Diagrama 2.0, y valores de esa clave listIndex: lista(list) de indices(int) de células que tienen neighbour_index vecinos en el Diagrama 2.0
                    if neighbour_init==cell_filter:
                        for index in listIndex: #Iteramos por valores, indices de cúlulas:[lista de cell index(int)] de la clave neighbour_index
                            res_2.append(self.cell_count.datas_all_prop[prop][index][4]) #Nos quedamos con el valor de la Propiedad: prop, de indice: index, y Diagrma 2.0: 4
                            if flagClasses:
                                areaUpTen=self.cell_count.area[index][4]+self.cell_count.area[index][4]*area_treshold
                                areaDownTen=self.cell_count.area[index][4]-self.cell_count.area[index][4]*area_treshold
                                if self.cell_count.area[index][-1]>areaUpTen:
                                    classesList.append('Increase')
                                    classesListNum.append(1)
                                elif self.cell_count.area[index][-1]<areaDownTen:
                                    classesList.append('Decrease')
                                    classesListNum.append(-1)
                                else:
                                    classesList.append('Nothing_Equal')
                                    classesListNum.append(0)
                flagClasses=False
                res.append(np.array(res_2))
                res_2_norm=norm(res_2)
                resNorm.append(np.array(res_2_norm))

            else: props.remove(prop)

        res.append(classesList) #To add class column
        res.append(classesListNum) #To add class column
        resNorm.append(classesList) #To add class column
        resNorm.append(classesListNum) #To add class column
        props=''.join(p.replace(' ','_')+',' for p in props) #To avoid space at propierties name
        props+='Class,ClassNum'
        f2=open('D2_All_'+str(cell_filter)+'_3ClassByArea_Treshold'+str(area_treshold*100)+'%_.csv', mode='x')
        f2.close()
        f2_norm=open('D2_All_'+str(cell_filter)+'_3ClassByArea_Treshold'+str(area_treshold*100)+'%_Norm.csv', mode='x')
        f2_norm.close()
        np.savetxt('D2_All_'+str(cell_filter)+'_3ClassByArea_Treshold'+str(area_treshold*100)+'%_.csv',np.transpose(res),delimiter=',',header=props, comments='',fmt='%s')
        np.savetxt('D2_All_'+str(cell_filter)+'_3ClassByArea_Treshold'+str(area_treshold*100)+'%_Norm.csv',np.transpose(resNorm),delimiter=',',header=props, comments='',fmt='%s')

    def extractDatas_All_can_filter_2ClassByArea_OneFiles(self,cell_filter):

        x,i,z= self.cell_count.cell_change_all_count()
        x1,i1,z1= self.cell_count.cell_count_never_change()

        props=self.cell_count.propierties[:]
        res=list()
        resNorm=list()
        flagClasses=True
        classesList=list()
        classesListNum=list()
        for prop in self.cell_count.propierties:
            res_2=list()

            if not(prop=='Neighbour' or prop=='Neighbour Descriptor'):

                """Cells Change"""
                for d in i[3:]: #Cogemos dict de celulas que cambian 3 o mas veces                ]
                    for neighbour_index,listIndex in d.items(): #Iteramos por las claves, las claves indican los vecinos que tienen esas células en el Diagrama 2.0, y valores de esa clave listIndex: lista(list) de indices(int) de células que tienen neighbour_index vecinos en el Diagrama 2.0
                        if neighbour_index==cell_filter: #Filtramos según el numero de vecinos en Diagrama 2.0
                            for index in listIndex:
                                res_2.append(self.cell_count.datas_all_prop[prop][index][4]) #Nos quedamos con el valor de la Propiedad: prop, de indice: index, y Diagrma 2.0: 4
                                if flagClasses:
                                    if self.cell_count.area[index][-1]>self.cell_count.area[index][4]:
                                        classesList.append('Increase')
                                        classesListNum.append(1)
                                    else:
                                        classesList.append('Decrease')
                                        classesListNum.append(-1)



                """Cells Never Change"""
                for neighbour_init,listIndex in i1.items():#Iteramos por las claves, las claves indican los vecinos que tienen esas células en el Diagrama 2.0, y valores de esa clave listIndex: lista(list) de indices(int) de células que tienen neighbour_index vecinos en el Diagrama 2.0
                    if neighbour_init==cell_filter:
                        for index in listIndex: #Iteramos por valores, indices de cúlulas:[lista de cell index(int)] de la clave neighbour_index
                            res_2.append(self.cell_count.datas_all_prop[prop][index][4]) #Nos quedamos con el valor de la Propiedad: prop, de indice: index, y Diagrma 2.0: 4
                            if flagClasses:
                                if self.cell_count.area[index][-1]>self.cell_count.area[index][4]:
                                    classesList.append('Increase')
                                    classesListNum.append(1)
                                else:
                                    classesList.append('Decrease')
                                    classesListNum.append(-1)


                flagClasses=False
                res.append(np.array(res_2))
                res_2_norm=norm(res_2)
                resNorm.append(np.array(res_2_norm))

            else: props.remove(prop)

        res.append(classesList) #To add class column
        res.append(classesListNum) #To add class column
        resNorm.append(classesList) #To add class column
        resNorm.append(classesListNum) #To add class column
        props=''.join(p.replace(' ','_')+',' for p in props) #To avoid space at propierties name
        props+='Class,ClassNum'
        f2=open('D2_All_'+str(cell_filter)+'_2ClassByArea_Treshold_.csv', mode='x')
        f2.close()
        f2_norm=open('D2_All_'+str(cell_filter)+'_2ClassByArea_Treshold_Norm.csv', mode='x')
        f2_norm.close()

        np.savetxt('D2_All_'+str(cell_filter)+'_2ClassByArea_Treshold_.csv',np.transpose(res),delimiter=',',header=props, comments='',fmt='%s')
        np.savetxt('D2_All_'+str(cell_filter)+'_2ClassByArea_Treshold_Norm.csv',np.transpose(resNorm),delimiter=',',header=props, comments='',fmt='%s')

h=count_Cells()


