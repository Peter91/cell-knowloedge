Call:
coxph(formula = Surv(Class) ~ Relation_Neighbour_Ratio_Axis + 
    Clustering_Coefficient + Relation_Neighbour_Angle + Area_Descriptor + 
    Relation_Neighbour_Major_Axis + Convex_Hull + Betweenness + 
    Relation_Neighbour_Area + Area + Area_Normalized + Average_NoN + 
    angle + Ratio_axis + Relation_Neighbour_Convex_Hull + Eccentricity + 
    Ratio_Descriptor + Minor_axis + Relation_Neighbour_Minor_Axis + 
    Major_axis + Strength + Centroid_Descriptor, data = d)

  n= 502, number of events= 502 

                                     coef  exp(coef)   se(coef)      z Pr(>|z|)   
Relation_Neighbour_Ratio_Axis   8.261e-01  2.284e+00  6.046e-01  1.366  0.17186   
Clustering_Coefficient         -6.446e-01  5.249e-01  7.467e-01 -0.863  0.38802   
Relation_Neighbour_Angle       -1.448e+00  2.351e-01  1.164e+00 -1.244  0.21362   
Area_Descriptor                -5.854e-01  5.569e-01  4.163e-01 -1.406  0.15961   
Relation_Neighbour_Major_Axis  -1.147e+00  3.175e-01  1.120e+00 -1.024  0.30572   
Convex_Hull                    -3.654e-01  6.939e-01  8.187e-01 -0.446  0.65540   
Betweenness                     2.733e-01  1.314e+00  3.960e-01  0.690  0.49003   
Relation_Neighbour_Area         1.085e+00  2.959e+00  1.710e+00  0.634  0.52581   
Area                            9.777e+00  1.762e+04  3.046e+00  3.210  0.00133 **
Area_Normalized                 8.505e+00  4.940e+03  3.333e+00  2.552  0.01072 * 
Average_NoN                    -7.909e-01  4.534e-01  2.904e-01 -2.723  0.00646 **
angle                           4.149e-02  1.042e+00  1.679e-01  0.247  0.80487   
Ratio_axis                      3.960e-01  1.486e+00  1.411e+00  0.281  0.77893   
Relation_Neighbour_Convex_Hull -3.217e-02  9.683e-01  6.962e-01 -0.046  0.96315   
Eccentricity                    2.234e-01  1.250e+00  3.961e-01  0.564  0.57273   
Ratio_Descriptor               -7.331e-02  9.293e-01  5.279e-01 -0.139  0.88956   
Minor_axis                     -9.309e+00  9.058e-05  3.795e+00 -2.453  0.01416 * 
Relation_Neighbour_Minor_Axis   3.535e-02  1.036e+00  1.527e+00  0.023  0.98153   
Major_axis                     -7.344e+00  6.466e-04  3.337e+00 -2.201  0.02774 * 
Strength                       -3.516e-01  7.035e-01  1.597e+00 -0.220  0.82570   
Centroid_Descriptor             3.542e-02  1.036e+00  2.747e-01  0.129  0.89738   
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

                               exp(coef) exp(-coef) lower .95 upper .95
Relation_Neighbour_Ratio_Axis  2.284e+00  4.378e-01 6.984e-01 7.472e+00
Clustering_Coefficient         5.249e-01  1.905e+00 1.215e-01 2.268e+00
Relation_Neighbour_Angle       2.351e-01  4.254e+00 2.400e-02 2.302e+00
Area_Descriptor                5.569e-01  1.796e+00 2.463e-01 1.259e+00
Relation_Neighbour_Major_Axis  3.175e-01  3.150e+00 3.533e-02 2.853e+00
Convex_Hull                    6.939e-01  1.441e+00 1.395e-01 3.453e+00
Betweenness                    1.314e+00  7.608e-01 6.049e-01 2.856e+00
Relation_Neighbour_Area        2.959e+00  3.380e-01 1.037e-01 8.444e+01
Area                           1.762e+04  5.674e-05 4.501e+01 6.901e+06
Area_Normalized                4.940e+03  2.024e-04 7.186e+00 3.396e+06
Average_NoN                    4.534e-01  2.205e+00 2.566e-01 8.011e-01
angle                          1.042e+00  9.594e-01 7.500e-01 1.449e+00
Ratio_axis                     1.486e+00  6.730e-01 9.359e-02 2.359e+01
Relation_Neighbour_Convex_Hull 9.683e-01  1.033e+00 2.474e-01 3.790e+00
Eccentricity                   1.250e+00  7.998e-01 5.752e-01 2.718e+00
Ratio_Descriptor               9.293e-01  1.076e+00 3.302e-01 2.615e+00
Minor_axis                     9.058e-05  1.104e+04 5.331e-08 1.539e-01
Relation_Neighbour_Minor_Axis  1.036e+00  9.653e-01 5.195e-02 2.066e+01
Major_axis                     6.466e-04  1.547e+03 9.340e-07 4.476e-01
Strength                       7.035e-01  1.421e+00 3.078e-02 1.608e+01
Centroid_Descriptor            1.036e+00  9.652e-01 6.048e-01 1.775e+00

Concordance= 0.754  (se = 0.041 )
Rsquare= 0.088   (max possible= 1 )
Likelihood ratio test= 46.33  on 21 df,   p=0.001155
Wald test            = 44.78  on 21 df,   p=0.001849
Score (logrank) test = 44.77  on 21 df,   p=0.001856

