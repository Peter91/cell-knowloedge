Call:
coxph(formula = Surv(Class) ~ Relation_Neighbour_Ratio_Axis + 
    Clustering_Coefficient + Relation_Neighbour_Angle + Area_Descriptor + 
    Relation_Neighbour_Major_Axis + Convex_Hull + Betweenness + 
    Relation_Neighbour_Area + Area + Area_Normalized + Average_NoN + 
    angle + Ratio_axis + Relation_Neighbour_Convex_Hull + Eccentricity + 
    Ratio_Descriptor + Minor_axis + Relation_Neighbour_Minor_Axis + 
    Major_axis + Strength + Centroid_Descriptor, data = d)

  n= 4716, number of events= 4716 

                                     coef  exp(coef)   se(coef)      z Pr(>|z|)    
Relation_Neighbour_Ratio_Axis    1.410689   4.098780   0.307073  4.594 4.35e-06 ***
Clustering_Coefficient          -0.351128   0.703894   0.261588 -1.342 0.179500    
Relation_Neighbour_Angle        -0.689225   0.501965   0.964824 -0.714 0.475009    
Area_Descriptor                 -0.584376   0.557453   0.156687 -3.730 0.000192 ***
Relation_Neighbour_Major_Axis   -0.597531   0.550168   0.415796 -1.437 0.150696    
Convex_Hull                      0.613802   1.847443   0.531710  1.154 0.248339    
Betweenness                      0.290190   1.336682   0.145519  1.994 0.046134 *  
Relation_Neighbour_Area          1.567104   4.792750   0.632578  2.477 0.013237 *  
Area                             6.212844 499.118727   1.195608  5.196 2.03e-07 ***
Area_Normalized                  5.104621 164.781536   1.096010  4.657 3.20e-06 ***
Average_NoN                     -0.817584   0.441497   0.133983 -6.102 1.05e-09 ***
angle                           -0.062302   0.939599   0.050292 -1.239 0.215419    
Ratio_axis                      -0.731378   0.481245   0.600022 -1.219 0.222875    
Relation_Neighbour_Convex_Hull  -0.281342   0.754770   0.457426 -0.615 0.538519    
Eccentricity                     0.125140   1.133307   0.116486  1.074 0.282693    
Ratio_Descriptor                 0.477838   1.612584   0.274575  1.740 0.081810 .  
Minor_axis                      -4.103877   0.016509   1.213991 -3.380 0.000724 ***
Relation_Neighbour_Minor_Axis   -1.213114   0.297270   0.572292 -2.120 0.034027 *  
Major_axis                      -6.475605   0.001541   1.533495 -4.223 2.41e-05 ***
Strength                        -1.193799   0.303068   0.654015 -1.825 0.067950 .  
Centroid_Descriptor              0.044851   1.045872   0.118381  0.379 0.704785    
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

                               exp(coef) exp(-coef) lower .95 upper .95
Relation_Neighbour_Ratio_Axis  4.099e+00  2.440e-01 2.245e+00 7.482e+00
Clustering_Coefficient         7.039e-01  1.421e+00 4.215e-01 1.175e+00
Relation_Neighbour_Angle       5.020e-01  1.992e+00 7.576e-02 3.326e+00
Area_Descriptor                5.575e-01  1.794e+00 4.100e-01 7.578e-01
Relation_Neighbour_Major_Axis  5.502e-01  1.818e+00 2.435e-01 1.243e+00
Convex_Hull                    1.847e+00  5.413e-01 6.516e-01 5.238e+00
Betweenness                    1.337e+00  7.481e-01 1.005e+00 1.778e+00
Relation_Neighbour_Area        4.793e+00  2.086e-01 1.387e+00 1.656e+01
Area                           4.991e+02  2.004e-03 4.792e+01 5.199e+03
Area_Normalized                1.648e+02  6.069e-03 1.923e+01 1.412e+03
Average_NoN                    4.415e-01  2.265e+00 3.395e-01 5.741e-01
angle                          9.396e-01  1.064e+00 8.514e-01 1.037e+00
Ratio_axis                     4.812e-01  2.078e+00 1.485e-01 1.560e+00
Relation_Neighbour_Convex_Hull 7.548e-01  1.325e+00 3.079e-01 1.850e+00
Eccentricity                   1.133e+00  8.824e-01 9.020e-01 1.424e+00
Ratio_Descriptor               1.613e+00  6.201e-01 9.415e-01 2.762e+00
Minor_axis                     1.651e-02  6.057e+01 1.529e-03 1.783e-01
Relation_Neighbour_Minor_Axis  2.973e-01  3.364e+00 9.683e-02 9.126e-01
Major_axis                     1.541e-03  6.491e+02 7.627e-05 3.112e-02
Strength                       3.031e-01  3.300e+00 8.411e-02 1.092e+00
Centroid_Descriptor            1.046e+00  9.561e-01 8.293e-01 1.319e+00

Concordance= 0.697  (se = 0.013 )
Rsquare= 0.054   (max possible= 1 )
Likelihood ratio test= 260.5  on 21 df,   p=0
Wald test            = 246.8  on 21 df,   p=0
Score (logrank) test = 247.3  on 21 df,   p=0

