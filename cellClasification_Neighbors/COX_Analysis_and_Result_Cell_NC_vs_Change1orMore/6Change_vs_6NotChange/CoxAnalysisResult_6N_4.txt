Call:
coxph(formula = Surv(Class) ~ Relation_Neighbour_Ratio_Axis + 
    Clustering_Coefficient + Relation_Neighbour_Angle + Area_Descriptor + 
    Relation_Neighbour_Major_Axis + Convex_Hull + Betweenness + 
    Relation_Neighbour_Area + Area + Average_NoN + angle + Ratio_axis + 
    Relation_Neighbour_Convex_Hull + Eccentricity + Ratio_Descriptor + 
    Minor_axis + Relation_Neighbour_Minor_Axis + Major_axis + 
    Strength + Centroid_Descriptor, data = d)

  n= 11454, number of events= 11454 

                                    coef exp(coef)  se(coef)       z Pr(>|z|)    
Relation_Neighbour_Ratio_Axis   1.325127  3.762663  0.190178   6.968 3.22e-12 ***
Clustering_Coefficient         -0.019313  0.980872  0.180563  -0.107   0.9148    
Relation_Neighbour_Angle        0.026445  1.026798  1.052725   0.025   0.9800    
Area_Descriptor                -0.088397  0.915397  0.110424  -0.801   0.4234    
Relation_Neighbour_Major_Axis  -0.700568  0.496303  0.269763  -2.597   0.0094 ** 
Convex_Hull                     0.922207  2.514835  0.549788   1.677   0.0935 .  
Betweenness                     0.201199  1.222868  0.107664   1.869   0.0617 .  
Relation_Neighbour_Area         1.972605  7.189377  0.373721   5.278 1.30e-07 ***
Area                            0.503691  1.654819  0.670882   0.751   0.4528    
Average_NoN                    -1.093591  0.335011  0.091868 -11.904  < 2e-16 ***
angle                          -0.010202  0.989850  0.031978  -0.319   0.7497    
Ratio_axis                     -2.463400  0.085145  0.423110  -5.822 5.81e-09 ***
Relation_Neighbour_Convex_Hull -0.326749  0.721265  0.515489  -0.634   0.5262    
Eccentricity                    0.150082  1.161930  0.073225   2.050   0.0404 *  
Ratio_Descriptor                0.190335  1.209654  0.267668   0.711   0.4770    
Minor_axis                      0.973945  2.648372  0.568580   1.713   0.0867 .  
Relation_Neighbour_Minor_Axis  -1.964883  0.140172  0.354110  -5.549 2.88e-08 ***
Major_axis                      0.621962  1.862579  0.556874   1.117   0.2640    
Strength                       -1.609800  0.199928  0.393106  -4.095 4.22e-05 ***
Centroid_Descriptor             0.008443  1.008479  0.072598   0.116   0.9074    
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

                               exp(coef) exp(-coef) lower .95 upper .95
Relation_Neighbour_Ratio_Axis    3.76266     0.2658   2.59189    5.4623
Clustering_Coefficient           0.98087     1.0195   0.68852    1.3974
Relation_Neighbour_Angle         1.02680     0.9739   0.13044    8.0829
Area_Descriptor                  0.91540     1.0924   0.73725    1.1366
Relation_Neighbour_Major_Axis    0.49630     2.0149   0.29250    0.8421
Convex_Hull                      2.51483     0.3976   0.85611    7.3874
Betweenness                      1.22287     0.8177   0.99023    1.5102
Relation_Neighbour_Area          7.18938     0.1391   3.45604   14.9556
Area                             1.65482     0.6043   0.44432    6.1632
Average_NoN                      0.33501     2.9850   0.27981    0.4011
angle                            0.98985     1.0103   0.92972    1.0539
Ratio_axis                       0.08514    11.7447   0.03715    0.1951
Relation_Neighbour_Convex_Hull   0.72126     1.3865   0.26261    1.9810
Eccentricity                     1.16193     0.8606   1.00659    1.3412
Ratio_Descriptor                 1.20965     0.8267   0.71585    2.0441
Minor_axis                       2.64837     0.3776   0.86897    8.0715
Relation_Neighbour_Minor_Axis    0.14017     7.1341   0.07002    0.2806
Major_axis                       1.86258     0.5369   0.62532    5.5479
Strength                         0.19993     5.0018   0.09253    0.4320
Centroid_Descriptor              1.00848     0.9916   0.87472    1.1627

Concordance= 0.674  (se = 0.009 )
Rsquare= 0.043   (max possible= 1 )
Likelihood ratio test= 507.7  on 20 df,   p=0
Wald test            = 480.1  on 20 df,   p=0
Score (logrank) test = 479.8  on 20 df,   p=0

