# -*- coding: utf-8 -*-
"""
Created on Thu Jan 22 11:08:47 2015

@author: pedro-xubuntu
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import ConvexHull
from scipy.spatial import Delaunay
from scipy.spatial import Voronoi,voronoi_plot_2d
import numpy.linalg as la
import operator

points=np.array([[ 116, 1791],
       [ 113, 1777],
       [  88, 1781],
       [  88, 1822],
       [ 100, 1830]])
centroid=(100,1800)

def vornoi(points):
    v=Voronoi(points)
    voronoi_plot_2d(v)

def polygon(points,centroid):
    hull=ConvexHull(points)
    plt.plot(centroid[0],centroid[1],'b*')
    for sim in hull.simplices:
        plt.plot(points[sim,0],points[sim,1],'r-')
    plt.plot(points[:,0],points[:,1],'b*')

def polygon_trian(points,centroid):
    plt.plot(centroid[0],centroid[1],'b*')
    tri = Delaunay(points)
    plt.triplot(points[:,0], points[:,1], tri.simplices.copy())
    plt.plot(points[:,0], points[:,1], 'o')


def angle(centroid,p2):

    v1=np.array([p2[0]-centroid[0],p2[1]-centroid[1]])
    v2=np.array([1.0,0.0])
    """ Returns the angle in radians between vectors 'v1' and 'v2'    """
    cosang = np.dot(v1, v2)
    sinang = la.norm(v1) * la.norm(v2)
    degree= np.arccos(cosang/sinang)
    print(p2,degree)
    if p2[1]>=centroid[1]:
        degree=2*np.pi-degree

    return degree

def polygon_angle(points,center):

    res=list()
    for p in range(len(points)):
        a=angle(center,points[p])
        res.append((p,a))
    res.sort(key=operator.itemgetter(1))
    edges=[(x[0],j[0]) for x,j in zip(res[:-1],res[1:])]
    edges.append((res[0][0],res[-1][0]))
    return edges
