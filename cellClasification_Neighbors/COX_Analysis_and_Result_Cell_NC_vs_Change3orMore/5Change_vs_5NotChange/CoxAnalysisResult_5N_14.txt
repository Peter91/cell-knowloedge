Call:
coxph(formula = Surv(Class) ~ Relation_Neighbour_Ratio_Axis + 
    Clustering_Coefficient + Relation_Neighbour_Angle + Area_Descriptor + 
    Relation_Neighbour_Major_Axis + Convex_Hull + Betweenness + 
    Relation_Neighbour_Area + Area + Area_Normalized + Average_NoN + 
    angle + Ratio_axis + Relation_Neighbour_Convex_Hull + Eccentricity + 
    Ratio_Descriptor + Minor_axis + Relation_Neighbour_Minor_Axis + 
    Major_axis + Strength + Centroid_Descriptor, data = d)

  n= 1320, number of events= 1320 

                                    coef exp(coef)  se(coef)      z Pr(>|z|)    
Relation_Neighbour_Ratio_Axis    1.56346   4.77531   0.48554  3.220  0.00128 ** 
Clustering_Coefficient           0.47629   1.61010   0.46015  1.035  0.30063    
Relation_Neighbour_Angle        -0.40367   0.66786   0.90943 -0.444  0.65713    
Area_Descriptor                 -0.08280   0.92054   0.18427 -0.449  0.65319    
Relation_Neighbour_Major_Axis   -1.10941   0.32975   0.73353 -1.512  0.13043    
Convex_Hull                      1.92098   6.82767   0.92959  2.066  0.03878 *  
Betweenness                      0.37968   1.46181   0.23690  1.603  0.10901    
Relation_Neighbour_Area          0.77897   2.17922   1.04205  0.748  0.45474    
Area                             4.39224  80.82147   1.80514  2.433  0.01497 *  
Area_Normalized                  4.61783 101.27404   1.46711  3.148  0.00165 ** 
Average_NoN                     -0.91270   0.40144   0.19291 -4.731 2.23e-06 ***
angle                           -0.04150   0.95935   0.09531 -0.435  0.66328    
Ratio_axis                      -2.39344   0.09132   1.23847 -1.933  0.05329 .  
Relation_Neighbour_Convex_Hull  -1.32434   0.26598   0.88328 -1.499  0.13378    
Eccentricity                     0.30675   1.35900   0.19637  1.562  0.11826    
Ratio_Descriptor                 0.23164   1.26067   0.34901  0.664  0.50688    
Minor_axis                      -3.02535   0.04854   2.19988 -1.375  0.16906    
Relation_Neighbour_Minor_Axis   -1.59235   0.20345   0.90279 -1.764  0.07777 .  
Major_axis                      -2.23136   0.10738   1.76717 -1.263  0.20671    
Strength                        -3.86094   0.02105   0.87604 -4.407 1.05e-05 ***
Centroid_Descriptor             -0.46436   0.62854   0.17808 -2.608  0.00912 ** 
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

                               exp(coef) exp(-coef) lower .95 upper .95
Relation_Neighbour_Ratio_Axis    4.77531   0.209411  1.843777   12.3678
Clustering_Coefficient           1.61010   0.621081  0.653391    3.9676
Relation_Neighbour_Angle         0.66786   1.497314  0.112351    3.9701
Area_Descriptor                  0.92054   1.086321  0.641495    1.3210
Relation_Neighbour_Major_Axis    0.32975   3.032574  0.078307    1.3886
Convex_Hull                      6.82767   0.146463  1.104093   42.2220
Betweenness                      1.46181   0.684082  0.918842    2.3256
Relation_Neighbour_Area          2.17922   0.458880  0.282687   16.7995
Area                            80.82147   0.012373  2.349590 2780.1068
Area_Normalized                101.27404   0.009874  5.710777 1795.9782
Average_NoN                      0.40144   2.491040  0.275052    0.5859
angle                            0.95935   1.042372  0.795879    1.1564
Ratio_axis                       0.09132  10.951083  0.008060    1.0345
Relation_Neighbour_Convex_Hull   0.26598   3.759715  0.047098    1.5021
Eccentricity                     1.35900   0.735835  0.924846    1.9970
Ratio_Descriptor                 1.26067   0.793231  0.636094    2.4985
Minor_axis                       0.04854  20.601153  0.000651    3.6195
Relation_Neighbour_Minor_Axis    0.20345   4.915278  0.034673    1.1937
Major_axis                       0.10738   9.312522  0.003363    3.4289
Strength                         0.02105  47.510057  0.003780    0.1172
Centroid_Descriptor              0.62854   1.590992  0.443349    0.8911

Concordance= 0.768  (se = 0.025 )
Rsquare= 0.098   (max possible= 1 )
Likelihood ratio test= 135.5  on 21 df,   p=0
Wald test            = 126.1  on 21 df,   p=0
Score (logrank) test = 126.7  on 21 df,   p=0

