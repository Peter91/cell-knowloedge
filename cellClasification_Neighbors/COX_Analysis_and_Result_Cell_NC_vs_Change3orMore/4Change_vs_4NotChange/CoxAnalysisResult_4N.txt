Call:
coxph(formula = Surv(Class) ~ Relation_Neighbour_Ratio_Axis + 
    Clustering_Coefficient + Relation_Neighbour_Angle + Area_Descriptor + 
    Relation_Neighbour_Major_Axis + Convex_Hull + Betweenness + 
    Relation_Neighbour_Area + Area + Area_Normalized + Average_NoN + 
    angle + Ratio_axis + Relation_Neighbour_Convex_Hull + Eccentricity + 
    Ratio_Descriptor + Minor_axis + Relation_Neighbour_Minor_Axis + 
    Major_axis + Strength + Centroid_Descriptor, data = d)

  n= 432, number of events= 432 

                                   coef exp(coef) se(coef)      z Pr(>|z|)  
Relation_Neighbour_Ratio_Axis   0.81042   2.24886  0.73491  1.103   0.2701  
Clustering_Coefficient         -1.09121   0.33581  0.94412 -1.156   0.2478  
Relation_Neighbour_Angle       -0.47078   0.62451  1.07391 -0.438   0.6611  
Area_Descriptor                -0.79149   0.45317  0.35730 -2.215   0.0267 *
Relation_Neighbour_Major_Axis  -2.09359   0.12324  1.06799 -1.960   0.0500 *
Convex_Hull                     1.77565   5.90409  1.90234  0.933   0.3506  
Betweenness                     0.38040   1.46286  0.45858  0.830   0.4068  
Relation_Neighbour_Area         1.70272   5.48887  1.47859  1.152   0.2495  
Area                            2.49964  12.17807  3.07783  0.812   0.4167  
Area_Normalized                 3.52721  34.02888  2.43107  1.451   0.1468  
Average_NoN                    -0.69515   0.49900  0.36798 -1.889   0.0589 .
angle                          -0.26635   0.76617  0.16235 -1.641   0.1009  
Ratio_axis                     -1.52276   0.21811  1.71549 -0.888   0.3747  
Relation_Neighbour_Convex_Hull -1.62377   0.19715  1.77523 -0.915   0.3604  
Eccentricity                   -0.03453   0.96606  0.26633 -0.130   0.8968  
Ratio_Descriptor                0.40505   1.49938  0.73266  0.553   0.5804  
Minor_axis                     -2.41858   0.08905  2.93028 -0.825   0.4092  
Relation_Neighbour_Minor_Axis  -1.24214   0.28877  1.15019 -1.080   0.2802  
Major_axis                     -0.87692   0.41606  2.52008 -0.348   0.7279  
Strength                       -1.74899   0.17395  1.65269 -1.058   0.2899  
Centroid_Descriptor            -0.62661   0.53440  0.36024 -1.739   0.0820 .
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

                               exp(coef) exp(-coef) lower .95 upper .95
Relation_Neighbour_Ratio_Axis    2.24886    0.44467 0.5326075    9.4955
Clustering_Coefficient           0.33581    2.97789 0.0527787    2.1366
Relation_Neighbour_Angle         0.62451    1.60124 0.0761071    5.1246
Area_Descriptor                  0.45317    2.20669 0.2249699    0.9128
Relation_Neighbour_Major_Axis    0.12324    8.11401 0.0151947    0.9996
Convex_Hull                      5.90409    0.16937 0.1418659  245.7129
Betweenness                      1.46286    0.68359 0.5954716    3.5937
Relation_Neighbour_Area          5.48887    0.18219 0.3026241   99.5549
Area                            12.17807    0.08211 0.0292230 5074.9613
Area_Normalized                 34.02888    0.02939 0.2900802 3991.8766
Average_NoN                      0.49900    2.00402 0.2425886    1.0264
angle                            0.76617    1.30520 0.5573494    1.0532
Ratio_axis                       0.21811    4.58486 0.0075587    6.2936
Relation_Neighbour_Convex_Hull   0.19715    5.07218 0.0060775    6.3956
Eccentricity                     0.96606    1.03513 0.5731988    1.6282
Ratio_Descriptor                 1.49938    0.66694 0.3566691    6.3031
Minor_axis                       0.08905   11.22994 0.0002853   27.7895
Relation_Neighbour_Minor_Axis    0.28877    3.46301 0.0303042    2.7516
Major_axis                       0.41606    2.40348 0.0029790   58.1101
Strength                         0.17395    5.74881 0.0068179    4.4381
Centroid_Descriptor              0.53440    1.87126 0.2637736    1.0827

Concordance= 0.807  (se = 0.044 )
Rsquare= 0.123   (max possible= 1 )
Likelihood ratio test= 56.65  on 21 df,   p=4.052e-05
Wald test            = 52.89  on 21 df,   p=0.0001425
Score (logrank) test = 53.61  on 21 df,   p=0.0001125

