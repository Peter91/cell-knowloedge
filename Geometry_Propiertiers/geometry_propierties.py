# -*- coding: utf-8 -*-
"""
Created on Thu Feb 26 17:37:07 2015

@author: equipo1luisma
"""
import numpy as np
import numpy.linalg as la
import operator


'''
Extract geometry propierties:

#####################################################

Triangle with neighbours:
    Area_Triangle/(Cell_Area*long_side_between_neigbours cell)
    
    Do triangles with all neighbours
    Calculate areas of each triangle
    get long of side
    
    Do the formula for each triangle
    
    get de maximun of the formula
    
#####################################################    
    

'''

def angle(p1,p2):

    v1 = np.array([p2[0]-p1[0],p2[1]-p1[1]])
    v2 = np.array([1.0,0.0])
    """ Returns the angle in radians between vectors 'v1' and 'v2'    """
    cosang = np.dot(v1, v2)
    sinang = la.norm(v1) * la.norm(v2)
    degree = np.arccos(cosang/sinang)

    if p2[1] >= p1[1]:
        degree=2*np.pi-degree
 
    return degree

def get_triangles(centroid, neighbours_centroid):
    
    res=list()
    for p in range(len(neighbours_centroid)):
        a = angle(centroid,neighbours_centroid[p])
        res.append((p,a))
    res.sort(key=operator.itemgetter(1))
    
    edges=[(neighbours_centroid[x[0]],neighbours_centroid[j[0]],np.array(centroid)) for x,j in zip(res[:-1],res[1:])]
    edges.append((neighbours_centroid[res[0][0]],neighbours_centroid[res[-1][0]],np.array(centroid)))
    
    return np.array(edges)

    
def get_area_triangle(triangle_vertex):
    
    """
    triangle_vertex:
       (vertex_A.vertex_B,vertex_Centroid)
    
    Formulas: 
        -> base_side*heigh_triangle * 0.5
        -> long_side_A*long_side_B*sin(angle between sida A and B)
    """
    
    side_A = np.array([triangle_vertex[1][0]-triangle_vertex[0][0],triangle_vertex[1][1]-triangle_vertex[0][1]])
    side_B = np.array([triangle_vertex[2][0]-triangle_vertex[0][0],triangle_vertex[2][1]-triangle_vertex[0][1]])
    side_C = np.array([triangle_vertex[2][0]-triangle_vertex[1][0],triangle_vertex[2][1]-triangle_vertex[1][1]])
    
    long_side_A = np.sqrt(np.sum(side_A**2))
    long_side_B = np.sqrt(np.sum(side_B**2))
    long_side_C = np.sqrt(np.sum(side_C**2))

    semiperimeter = (long_side_A + long_side_B + long_side_C) / 2.0
    
    heigh_triangle = semiperimeter/long_side_B * np.sqrt( semiperimeter * (semiperimeter - long_side_A) * (semiperimeter - long_side_B) * (semiperimeter - long_side_C) )
    
    area_triangle= long_side_B*heigh_triangle * 0.5
    
    return area_triangle
    
def get_long_side( triangle_vertex, cell_dict_vertex):
    
    centroid_A = triangle_vertex[0]
    centroid_B = triangle_vertex[1]

    """
    Get vertexs of centroid A -> VA
    Get vertexs of centroid B -> VB
    VA U VB -> vertex in common of centroid A and B
    Vector of two vertex in common
    Long of vector    
    
    """
   
    va = set([tuple(x) for x in cell_dict_vertex[tuple(centroid_A)]])
    vb = set([tuple(x) for x in cell_dict_vertex[tuple(centroid_B)]])
    
    vertex_join = va.intersection(vb)
    vertex_join = np.array(list(vertex_join))
    #print( vertex_join)
   
    side_vector = np.array([vertex_join[1][0]-vertex_join[0][0],vertex_join[1][1]-vertex_join[0][1]])       
    
    side_long = np.sqrt(np.sum(side_vector**2))
    return side_long
    
    
    
    

def formula_triangle_side(tri_area, cell_area , long_side) :
    return tri_area/(cell_area * long_side)    
    

def cell_vertex_data(image):
    
    with open('/home/equipo1luisma/Workspace/Cell_Knowledge/Small_Graphs_Analysis/VertexData_and_ValidCells/Vertex_Center_Image'+str(image)+'.csv') as f:  
    #Here open the file that contain the vertex(column-row) in pixel of each cell
        f_data=f.readlines() #Read all files, getting it in a list of list, each line a list
        f_data=[i.replace('\n','').split(',') for i in f_data]

    """Get Valid Cells"""
    with open('/home/equipo1luisma/Workspace/Cell_Knowledge/Small_Graphs_Analysis/VertexData_and_ValidCells/Matrix_adjacency_Image_'+str(image)+'.csv') as f:
        f_valid_cells=f.readlines()
        f_valid_cells=[i.split(',')  for i in f_valid_cells[2:]]
    
    valid_cells_set=set()
    for i in f_valid_cells:
        valid_cells_set.add(int(i[2]))
   
   
    """
    I'm going to storage data in a dict: key-value
    Key: Is cell name in a string, ex: 'Cell_1', or 'Cell_5'
    Value: Is a numpy nd-array that storage the vertex(Col,Row) in pixel of a cell,like that: [ [col,row],[col,row],...[col,row]]
    """

    col_init=2
    col_end=17
    #diagrams=['1.00', '1.25', '1.50', '1.75', '2.00', '2.25', '2.50', '2.75', '3.00', '3.25', '3.50', '3.75', '4.00', '4.25', '4.50', '4.75', '5.00', '5.25', '5.50', '5.75', '6.00']
    res_vertex = list() #List with all the vertex of each cell, one per diagram
    res_centroid = list()
    res_neighbours_valid_cells = list() #List with all the neighbours of each cell, one per diagram
    res_centroid_vertex = list()    
    for diagram_num in range(21):
    
        cellVertexData = dict() #Create the data structure
        cellCentroidData = dict()  
        cellNeighboursData = dict()
        cellCentroidVertexData = dict()
        for row_line,col_line in zip(range(2,4002,2),range(3,4002,2)): #Here Iterate over index of the file, because in the file the information is writen for each cell in two lines, even lines - row index, odd lines - column index
        
            row_line = f_data[row_line][col_init:col_end] #Split the line
            col_line = f_data[col_line][col_init:col_end]
            
            key_name = row_line[0]+'_'+col_line[0] #Save the name-key
            
            centroid = (int(col_line[2])-1,int(row_line[2])-1)
            values_vertex_index = [np.array([int(col)-1,int(row)-1]) for col,row in zip(col_line[3:],row_line[3:]) if col!='0' and row!='0']  #creating values-2d-array, avoiding vertex with index 0, this is beacause there are cell with different number of vertex and nodes. Index 0,0 means that no exist/there aren't more vertex
            
            values_vertex_index = np.array(values_vertex_index) #Convert to numpy array
            cellVertexData[key_name] = values_vertex_index #Save into data structure the cell vertex
            cellCentroidData[key_name] =  centroid
            
            cellCentroidVertexData[centroid] = values_vertex_index
        """ Neighbours of Cell"""    
        for row_line in f_valid_cells:
            
            row_line = row_line[col_init:col_end]
            key_name = 'Cell_'+row_line[0] #Save the name-key
            values_neighbours = [int(neighbours) for neighbours in row_line if neighbours!='0']
            values_neighbours = np.array(values_neighbours)
            cellNeighboursData[key_name] = values_neighbours
            
        col_init+=17
        col_end+=17
        
        
        res_vertex.append(cellVertexData)
        res_centroid.append(cellCentroidData)
        res_neighbours_valid_cells.append(cellNeighboursData)
        res_centroid_vertex.append(cellCentroidVertexData)
        
    return res_vertex, res_centroid, res_neighbours_valid_cells, valid_cells_set,res_centroid_vertex  #Return the list that contain all the graphs of each of one image, one per diagram
    #return res_centroid_vertex


    
if __name__ == "__main__":

    import pickle
    import Small_Graphs_Analysis.build_MasterGraph as bmg
    import matplotlib.pyplot as plt
    
    def get_new_prop_one_cell():
    
   
        cell_areas = pickle.load( open('bin/area.p','rb') )
        count_cell=0
        
        max_area_triangle = list()
        min_area_triangle = list()
        formula_area_triangle = list()
        side_max_long = list()
        side_min_long = list()
        side_formula_long = list()
        formula_max_value = list()
        formula_min_value = list()
        
        for image_x in range(1,2):
            print('Image: ',image_x)
            #cell_vertex_data_list = pickle.load(open('bin/vertex_image_list'+str(image_x)+'.p','rb'))
            cell_centroid_data_list = pickle.load(open('bin/centroids_image_list'+str(image_x)+'.p','rb'))
            cell_neighbours_data_list = pickle.load(open('bin/neighbours_image_list'+str(image_x)+'.p','rb'))
            valid_cells = pickle.load(open('bin/valid_cells_set_image'+str(image_x)+'.p','rb'))
            valid_cells = np.array( list(valid_cells) )
            centroid_vertex_data_list = pickle.load(open('bin/centroid_vertex_image_list'+str(image_x)+'.p','rb'))
            
            for cell_x in valid_cells:                
                
                count_cell+=1
                max_area_triangle_d = np.zeros(21,dtype=np.int)
                min_area_triangle_d = np.zeros(21,dtype=np.int)
                formula_area_triangle_d = np.zeros(21,dtype=np.int)
                side_max_long_d = np.zeros(21,dtype=np.int)
                side_min_long_d = np.zeros(21,dtype=np.int)
                side_formula_long_d = np.zeros(21,dtype=np.int)
                formula_max_value_d = np.zeros(21,dtype=np.int)
                formula_min_value_d = np.zeros(21,dtype=np.int)
                
                for diagram_x in range(21):                   
                    print('diagrama: ',diagram_x)
                    cell_name = 'Cell_'+str(cell_x)
                    n_centroid = np.array([ cell_centroid_data_list[diagram_x]['Cell_'+str(n)] for n in cell_neighbours_data_list[diagram_x][cell_name][1:]])
                    c_centroid = cell_centroid_data_list[diagram_x][cell_name]
                    
                    
                    """Get all triangles(vertex) of a cell"""
                    tri = get_triangles( c_centroid, n_centroid)
                    tri_areas = list()
                    long_sides = list()
                    for t_vertex in tri:
                        """Get all areas of triangles"""
                        tri_areas.append( get_area_triangle(t_vertex) )
                        """Get all long side of triangles"""
                        long_sides.append( get_long_side(t_vertex, centroid_vertex_data_list[diagram_x]) )
                    
                    formula = list()
                    
                    for tri_area,long_side in zip(tri_areas,long_sides):                     
                        """Get Formula value"""
                        prop = formula_triangle_side(tri_area , cell_areas[count_cell][diagram_x] , long_side)
                        
                        formula.append(prop)
                        
                    """
                    maximun_area_triangles = max(tri_areas)
                    minimun_area_triangles = min(tri_areas)
                    maximun_side_long = max(long_sides)
                    minimun_side_long = min(long_sides)
                    maximun_formula = max(formula)
                    minimun_formula = min (formula)
                    formula_side = long_sides[ formula.index(maximun_formula)] 
                    area_triangle_formula = tri_areas[ formula.index(maximun_formula)]
                    """
                    max_area_triangle_d[diagram_x] = max(tri_areas)
                    min_area_triangle_d[diagram_x] = min(tri_areas)
                    side_max_long_d[diagram_x] = max(long_sides)
                    side_min_long_d[diagram_x] = max(long_sides)                    
                    formula_max_value_d[diagram_x] = max(formula)
                    formula_min_value_d[diagram_x] = min(formula)
                    
                    maximun_formula = max(formula)
                    formula_area_triangle_d[diagram_x] = tri_areas[ formula.index(maximun_formula)]
                    side_formula_long_d[diagram_x] = long_sides[ formula.index(maximun_formula)] 
                    
                """Save data in upper list"""
                max_area_triangle.append(max_area_triangle_d)
                min_area_triangle.append(min_area_triangle_d)
                side_max_long.append(side_max_long_d)
                side_min_long.append(side_min_long_d)
                formula_max_value.append(formula_max_value_d)
                formula_min_value.append(formula_min_value_d)
                
                formula_area_triangle.append(formula_area_triangle_d)
                side_formula_long.append(side_formula_long_d)
                
        max_area_triangle = np.array(max_area_triangle)
        min_area_triangle = np.array(min_area_triangle)
        formula_area_triangle = np.array(formula_area_triangle)
        side_max_long = np.array(side_max_long)
        side_min_long = np.array(side_min_long)
        side_formula_long = np.array(side_formula_long)
        formula_max_value = np.array(formula_max_value)
        formula_min_value = np.array(formula_min_value)
        
        np.savetxt('max_area_tri.csv',max_area_triangle, fmt='%S', delimiter=',', header='', comments='')
        np.savetxt('min_area_tri.csv',min_area_triangle, fmt='%S', delimiter=',', header='', comments='')
        np.savetxt('formula_area_triangle.csv',formula_area_triangle, fmt='%S', delimiter=',', header='', comments='')
        np.savetxt('side_max_long.csv',side_max_long, fmt='%S', delimiter=',', header='', comments='')
        np.savetxt('side_min_long.csv',side_min_long, fmt='%S', delimiter=',', header='', comments='')
        np.savetxt('side_formula_long.csv',side_formula_long, fmt='%S', delimiter=',', header='', comments='')
        np.savetxt('formula_max_value.csv',formula_max_value, fmt='%S', delimiter=',', header='', comments='')
        np.savetxt('formula_min_value.csv',formula_min_value, fmt='%S', delimiter=',', header='', comments='')
                
    
    def test():
        mg= pickle.load( open( '/home/equipo1luisma/Workspace/Cell_Knowledge/Small_Graphs_Analysis/bin/master_graphs_image_1.p', 'rb' ) )
        mgd1=mg[0]
        bmg.plot_graph_original_position(mgd1)
        centroid= (94,2048-186)
        n_centroid=[(56,2048-194),(83,2048-243),(84,2048-137),(128,2048-175),(140,2048-238)]
        plt.plot(centroid[0],centroid[1],'r+')
        plt.text(centroid[0],centroid[1],'('+str(centroid[0])+','+str(centroid[1])+')')
        nc=np.array(n_centroid)
        plt.plot(nc[:,0],nc[:,1],'b+')
        for x,y in nc: plt.text(x,y, '('+str(x)+','+str(y)+')')
    
        ncc=np.array([[  56, 1854],[  83, 1805],[  84, 1911],
             [ 128, 1873],
             [ 140, 1810],
             [ 94,1862]])
        plt.triplot(ncc[:,0],ncc[:,1])
    get_new_prop_one_cell()        
#    for image in range(1,21):
#        print( image )
#        vertexs, centroid, neighbours, valid_cells_set,centroid_vertex = cell_vertex_data(image)
#        pickle.dump(centroid_vertex, open('centroid_vertex_image_list'+str(image)+'.p','wb'))
#        #vertexs, centroid, neighbours, valid_cells_set= cell_vertex_data(image)
#        
#        pickle.dump(vertexs, open('vertex_image_list'+str(image)+'.p','wb'))
#        pickle.dump(centroid, open('centroids_image_list'+str(image)+'.p','wb'))
#        pickle.dump(neighbours, open('neighbours_image_list'+str(image)+'.p','wb'))
#        pickle.dump(valid_cells_set, open('valid_cells_set_image'+str(image)+'.p','wb'))
#        
#        
#    v = pickle.load(open('vertex_image_list1.p','rb'))
#    c = pickle.load(open('centroids_image_list1.p','rb'))
#    n = pickle.load(open('neighbours_image_list1.p','rb'))
#    vc = pickle.load(open('valid_cells_set_image1.p','rb'))