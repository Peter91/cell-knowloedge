Call:
coxph(formula = Surv(Class) ~ Relation_Neighbour_Ratio_Axis + 
    Clustering_Coefficient + Relation_Neighbour_Angle + Area_Descriptor + 
    Relation_Neighbour_Major_Axis + Convex_Hull + Betweenness + 
    Relation_Neighbour_Area + Area + Area_Normalized + Average_NoN + 
    angle + Ratio_axis + Relation_Neighbour_Convex_Hull + Eccentricity + 
    Ratio_Descriptor + Minor_axis + Relation_Neighbour_Minor_Axis + 
    Major_axis + Strength + Centroid_Descriptor, data = d)

  n= 4716, number of events= 4716 

                                     coef  exp(coef)   se(coef)      z Pr(>|z|)    
Relation_Neighbour_Ratio_Axis   1.269e+00  3.557e+00  3.091e-01  4.106 4.03e-05 ***
Clustering_Coefficient         -8.383e-02  9.196e-01  2.469e-01 -0.340   0.7342    
Relation_Neighbour_Angle       -7.122e-01  4.906e-01  9.725e-01 -0.732   0.4640    
Area_Descriptor                -5.257e-01  5.911e-01  1.454e-01 -3.616   0.0003 ***
Relation_Neighbour_Major_Axis  -2.141e-01  8.073e-01  4.116e-01 -0.520   0.6030    
Convex_Hull                     5.647e-01  1.759e+00  4.116e-01  1.372   0.1701    
Betweenness                     3.572e-01  1.429e+00  1.452e-01  2.459   0.0139 *  
Relation_Neighbour_Area         1.155e+00  3.174e+00  7.025e-01  1.644   0.1002    
Area                            6.700e+00  8.123e+02  1.130e+00  5.927 3.09e-09 ***
Area_Normalized                 6.310e+00  5.499e+02  1.234e+00  5.114 3.16e-07 ***
Average_NoN                    -8.353e-01  4.337e-01  1.340e-01 -6.232 4.60e-10 ***
angle                          -4.164e-02  9.592e-01  5.006e-02 -0.832   0.4054    
Ratio_axis                     -1.033e-01  9.019e-01  6.029e-01 -0.171   0.8640    
Relation_Neighbour_Convex_Hull -5.133e-01  5.985e-01  3.527e-01 -1.455   0.1456    
Eccentricity                    1.185e-01  1.126e+00  1.160e-01  1.021   0.3071    
Ratio_Descriptor                3.979e-01  1.489e+00  2.684e-01  1.482   0.1383    
Minor_axis                     -4.982e+00  6.860e-03  1.225e+00 -4.067 4.77e-05 ***
Relation_Neighbour_Minor_Axis  -4.583e-01  6.324e-01  5.420e-01 -0.846   0.3978    
Major_axis                     -7.800e+00  4.098e-04  1.471e+00 -5.303 1.14e-07 ***
Strength                       -5.702e-01  5.654e-01  6.317e-01 -0.903   0.3668    
Centroid_Descriptor             1.757e-02  1.018e+00  9.735e-02  0.180   0.8568    
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

                               exp(coef) exp(-coef) lower .95 upper .95
Relation_Neighbour_Ratio_Axis  3.557e+00  2.811e-01 1.941e+00 6.519e+00
Clustering_Coefficient         9.196e-01  1.087e+00 5.668e-01 1.492e+00
Relation_Neighbour_Angle       4.906e-01  2.038e+00 7.293e-02 3.300e+00
Area_Descriptor                5.911e-01  1.692e+00 4.445e-01 7.860e-01
Relation_Neighbour_Major_Axis  8.073e-01  1.239e+00 3.603e-01 1.809e+00
Convex_Hull                    1.759e+00  5.685e-01 7.850e-01 3.941e+00
Betweenness                    1.429e+00  6.997e-01 1.075e+00 1.900e+00
Relation_Neighbour_Area        3.174e+00  3.151e-01 8.009e-01 1.258e+01
Area                           8.123e+02  1.231e-03 8.862e+01 7.446e+03
Area_Normalized                5.499e+02  1.818e-03 4.898e+01 6.174e+03
Average_NoN                    4.337e-01  2.305e+00 3.335e-01 5.641e-01
angle                          9.592e-01  1.043e+00 8.696e-01 1.058e+00
Ratio_axis                     9.019e-01  1.109e+00 2.767e-01 2.940e+00
Relation_Neighbour_Convex_Hull 5.985e-01  1.671e+00 2.998e-01 1.195e+00
Eccentricity                   1.126e+00  8.883e-01 8.968e-01 1.413e+00
Ratio_Descriptor               1.489e+00  6.718e-01 8.797e-01 2.519e+00
Minor_axis                     6.860e-03  1.458e+02 6.216e-04 7.571e-02
Relation_Neighbour_Minor_Axis  6.324e-01  1.581e+00 2.186e-01 1.829e+00
Major_axis                     4.098e-04  2.440e+03 2.294e-05 7.321e-03
Strength                       5.654e-01  1.769e+00 1.639e-01 1.950e+00
Centroid_Descriptor            1.018e+00  9.826e-01 8.409e-01 1.232e+00

Concordance= 0.691  (se = 0.013 )
Rsquare= 0.051   (max possible= 1 )
Likelihood ratio test= 247.1  on 21 df,   p=0
Wald test            = 235.3  on 21 df,   p=0
Score (logrank) test = 235.9  on 21 df,   p=0

